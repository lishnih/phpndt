<?php // Stan 2006-10-10
  // При загрузке библиотека определяет положение директории скрипта
  // относительно корневого url и загружает необходимые библиотеки.
  // Также начинается кеширование.
  // Функция print_contents вызывается в конце html страницы и выводит
  // содержимое буфера

if ( !defined( 'SCRIPT_DIR' ) ) {
  define( 'SCRIPT_DIR', dirname( __FILE__ ) );

  date_default_timezone_set('Europe/Moscow');
  header( "Content-Type: text/html; charset=utf-8" );

//   echo $_SERVER["SCRIPT_NAME"] . '<br />';
//   echo $_SERVER["REQUEST_URI"] . '<br />';
//   echo $_SERVER["DOCUMENT_URI"] . '<br />';
//   echo $_SERVER["SCRIPT_FILENAME"] . '<br />';
//   echo $_SERVER["PHP_SELF"] . '<br />';
//   echo '<br />';

  $str_uri    = $_SERVER["DOCUMENT_URI"];
  $str_file   = str_replace( "\\", "/", $_SERVER["SCRIPT_FILENAME"]);
  $str_script = str_replace( "\\", "/", SCRIPT_DIR);
// используем PCRE, из-за буквы диска в Win32 - можеть быть Заглавной/строчной
  $str_script = str_replace( "/", "\\/", $str_script );
  $rel_path   = preg_split( "/$str_script/i", $str_file );
  $rel_path   = explode( $rel_path[1], $str_uri );
  $script_uri = $rel_path[0];

  if ( !isset( $rel_path[1] ) ) {
    echo $str_uri . ' <=> ' . $str_file . '<br />';
    echo           'x <=> ' . $str_script . '<br />';
    print_r( $script_uri );
    echo '<br />';
    die("Не получается определить локальный путь!");
  };
  define( 'SCRIPT_URL', $script_uri );


  include SCRIPT_DIR . '/lib/common.php';
  include SCRIPT_DIR . '/config/main.php';
  include SCRIPT_DIR . '/includes/start.php';

//print_rb($str1, $str2, $str3, SCRIPT_URL);
  ob_start();

  function phpndt_exit_handler ( ) {
    $content = ob_get_contents();
    ob_end_clean();
  
    include 'html/header.php';
    echo $content;
    include 'html/footer.php';
  }; // function

  register_shutdown_function( 'phpndt_exit_handler' );
} // if
?>
