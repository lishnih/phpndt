<?php	// Stan 27 января 2009г.

/* Описание базы данных для phpNDT

Название таблицы | Ключ | Ссылка на другую [таблицу]
  reports          id
  joints           id
  mjoints          -      _reports_id_
                          _joints_id
  rt               -      _reports_id
  ut               -      _reports_id
  mt               -      _reports_id
*/

  // Параметры подключения
//   $db_conf['type']         = 'sqlite';
//   $db_conf['host']         = '/home';

  $db_conf['type']         = 'mysql';
  $db_conf['host']         = 'localhost';

  $db_conf['name']         = 'pgu235';

  $db_conf['user']         = 'root';    // для Sqlite не используется
  $db_conf['passwd']       = '54321';   // для Sqlite не используется
  $db_conf['table_prefix'] = '';

  // Перечислите здесь все таблицы, в которыми будет работать скрипт
  // Ветки с такими именами автоматически будут извлекаться из $_GET
  $db_conf['alltables'] = 'reports,register_entries,joints';

  $db_conf['tasks']['key'] = 'id';
  $db_conf['tasks']['relative_keys']['dirs'] = '_tasks_id';

  $db_conf['dirs']['key'] = 'id';
  $db_conf['dirs']['relative_keys']['files'] = '_dirs_id';

  $db_conf['files']['key'] = 'id';
  $db_conf['files']['relative_keys']['sheets'] = '_files_id';

  $db_conf['sheets']['key'] = 'id';
  $db_conf['sheets']['relative_keys']['register_entries'] = '_sheets_id';

  $db_conf['reports']['key'] = 'id';
  $db_conf['reports']['relative_keys']['register_entries'] = '_reports_id';

  $db_conf['joints']['key'] = 'id';
  $db_conf['joints']['relative_keys']['register_entries'] = '_joints_id';
?>
