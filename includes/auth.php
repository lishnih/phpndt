<?php // Stan 10 октября 2006г.

// Запрашивает аутентификацию
function auth ( ) {
  header( 'WWW-Authenticate: Basic realm="NDT ' . str_replace( '\'', '\\\'', $_SERVER['HTTP_HOST'] ) . '"' );
  header( 'HTTP/1.0 401 Unauthorized' );
  header( 'status: 401 Unauthorized' );
  return false;
} // function

// Возращает текущего пользователя
function current_user ( $required = 1 ) {
  // Таблица пользователей
  require 'authusers.php';

  // Если пользователь аутенфицировался
  if ( isset( $_SERVER['PHP_AUTH_USER'] ) AND $_SERVER['PHP_AUTH_USER'] AND $_SERVER['PHP_AUTH_PW'] ) {
    // ищем его в таблице
    foreach ( $users as $key => $user )
      if ( $user[0] == $_SERVER['PHP_AUTH_USER'] AND $user[1] == $_SERVER['PHP_AUTH_PW'] ) {
        return $user;
      }; // if
  // Если пользователь не аутенфицировался, но требуется, то аутенфицируем
  } elseif ( $required ) {
    auth();
    user_error( 'Требуется аутентификация!', ERROR );
    exit();
  } else
    return False;
} // function

// Гость или зарегистрированный пользователь
function is_registered ( $user ) {
  return $user[2] > 0 ? 1 : 0;
} // function

// Проверяет пользователя
// $user - пользователь
// $active - если установлена, то будет запрашиваться аутентификация если доступ запрещён
// $check_module - проверка модуля
// $check_rights - проверка доступа к модулю
function check_user ( $user, $active = 1, $check_module = '', $check_rights = '' ) {
  list( $login, $pass, $id, $name, $module, $rights, $host ) = $user;
  $str = '';

  // Проверяем доступ к модулю если требуется
  if ( $check_module AND $module != '*' AND !stristr( $module, $check_module ) )
      $str .= "Для данного пользователя доступ к данному модулю не разрешён ($module, $check_module)!<br />
<a href=\"login.php\">Изменить пользователя</a><br />\n";

  // Проверяем привилегии в модуле если требуется
  if ( $check_rights AND $rights != '*' AND !stristr( $rights, $check_rights ) )
      $str .= "Нет соответствующих разрешений на модуль ($rights, $check_rights)!<br />
<a href=\"login.php\">Изменить пользователя</a><br />\n";

  // Проверяем хост с которого пришёл пользователь
  $host_flag = 0;
  switch ( $host ) {
    case '*':
      $host_flag = 1;
      break;
    case 'localhost':
      if ( $_SERVER["REMOTE_ADDR"] == '127.0.0.1' )
        $host_flag = 1;
      break;
    default:
      if ( $_SERVER["REMOTE_ADDR"] == $host )
        $host_flag = 1;
      break;
  }; // switch

  if ( !$host_flag )
    $str .= "Для данного пользователя доступ с данного узла не разрешён!<br />
<a href=\"login.php\">Изменить пользователя</a><br />\n";

  if ( $active AND $str ) {
    auth();
    user_error( $str, ERROR );
    exit();
  }; // if

  return $str;
} // function
?>
