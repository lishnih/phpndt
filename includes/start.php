<?php // Stan 23 января 2009г.

  require SCRIPT_DIR . '/includes/auth.php';
  $user = current_user( 0 );  // Инициализируем пользователя

  include SCRIPT_DIR . '/config/db_config/ndt.php';

  include SCRIPT_DIR . '/lib/meta_sql.php';    // Работа с БД через meta_array
  include SCRIPT_DIR . '/lib/view.php';        // Функции вывода

  // Специфические библиотеки для пакета
  include SCRIPT_DIR . '/lib/sprint.php';


  // Извлекаем зарезервированные переменные
  $mode       = isset( $_GET['mode']       )   ? $_GET['mode']       : '';
  $show_table = isset( $_GET['show_table'] )   ? $_GET['show_table'] : '';
  $options    = isset( $_GET['options']    )   ? $_GET['options']    : '';
  $options    = explode( ',', $options );

  // Здесь определяются таблицы, с которыми будет работать выборка
  // $get_init['reports'] = array( 'enabled' => 1 );

  // $get_diff['reports'] = array_diff( $meta_get->get('reports'), $get_init['reports'] );
  // $get_diff['joints']  = array_diff( $meta_get->get('joints'),  $get_init['joints']  );
  // $get_diff['mjoints'] = array_diff( $meta_get->get('mjoints'), $get_init['mjoints'] );

  define( 'BOTH_SORTING',    'date,report,report_str,number' );
  define( 'REPORTS_SORTING', 'report.kind,report.diameter,report.date,report.report,report.sign,report.report_str' );
  define( 'BUTTS_SORTING',   'joints.diameter,joints.kp,joints.type,joints.number,joints.sign' );
?>
