<?php // Stan 2007-04-05
      //      2009-02-19


function sql_open_db ( $db_host, $db_user, $db_passwd, $db_name ) {
  if ( !$conn = mysql_connect( $db_host, $db_user, $db_passwd ) )
    user_error( mysql_errno().': '.mysql_error(), ERROR );
  if ( !mysql_select_db( $db_name, $conn ) )
    user_error( mysql_errno().': '.mysql_error(), ERROR );
  mysql_query( 'SET NAMES UTF8' );
  return $conn;
} // function


function sql_query ( $sqlquery ) {
  return mysql_query( $sqlquery );
} // function


function sql_fetch_array ( $result ) {
  if ( ! is_resource( $result ) ) {
    user_error( "Запрос не выполнился!" );
    return False;
  } // if
  return mysql_fetch_array( $result, MYSQL_ASSOC );
} // function


function sql_fetch_row ( $result ) {
  if ( ! is_resource( $result ) ) {
    user_error( "Запрос не выполнился!" );
    return False;
  } // if
  return mysql_fetch_row( $result );
} // function
?>
