<?php // Stan 2007-04-05
      //      2009-02-19

$db = '';

function sql_open_db ( $db_host, $db_user, $db_passwd, $db_name ) {
global $db;
  if ( !$db = new SQLite3( $db_name ) )
    user_error( 'база не открылась!', ERROR );
} // function


function sql_query ( $sqlquery ) {
global $db;
  return $db->query( $sqlquery );
} // function


function sql_fetch_array ( $result ) {
  return $result->fetchArray( SQLITE3_ASSOC );
} // function


function sql_fetch_row ( $result ) {
  return $result->fetchArray( SQLITE3_ASSOC );
} // function
?>
