<?php // Stan May 17, 2008
      //      February 16, 2009

/**
* @package      phpNDT
* @name         MetaArray
* @version      1.1
* @date         19.Feb.2009
*
* @author       Stan
* @email        lishnih@gmail.com
* @copyright    2006—2009
*/

// Класс MetaArray
// Данный класс работает в связке с массивом $_GET и предназначен
// для хранения равенств и условий переданных через данный массив по
// специальному (своему) протоколу


// В Python'е довольно удобно описываются функции в классах.
// Как сделать это в PHP4/5, чтобы вызывать функцию рекурсивно,
// я не понял, может что-то не понимаю, но рекурсивные функции
// пришлось вынести за пределы класса


// Эта функция определяет, является ли массив условием
// '>' => '100', но не вложенное условие 'key' => '>' => '100'
// BEUSEDBY{serialize_array_sql}
function is_meta_condition ( $array ) {
  if ( !is_array( $array ) )
    return False;
  $key = key( $array );
  return strstr( '<<>>!~%*', $key ) ? $key : False;
  # !!! проверяет только первый элемент (может быть < и > - что работает,
  # благодаря доделке в serialize_array_sql, но чуть-чуть кривовато получается)
} // function


// Разбирает условия для SQL
// $key - ключ, $val - значение ключа,
// $condition - условие между $key и $val
// BEUSEDBY{serialize_array_sql}
function prepare_condition ( $key, $condition, $val ) {
  switch ( $condition ) {
    case '<':  $condition = '<';      break;
    case '<<': $condition = '<=';     break;
    case '>':  $condition = '>';      break;
    case '>>': $condition = '>=';     break;
    case '!':  $condition = '!=';     break;
    case '~':  $condition = ' LIKE '; break;
    case '%':  $condition = '%';      break;
    case '*':                   // Сложное условие ИЛИ
      $condition = '';
      $vals = explode( '|', $val );
      for ( $i = 0; $i < count( $vals ); $i++ ) {
        if ( $condition )
          $condition .= ' OR ';
        $condition .= "$key='$vals[$i]'";
      }; // for
      return '(' . $condition . ')';
    default:   $condition = '=';
               user_error( 'Неверное условие!' );
  }; // switch
  if ( $condition == '%' )
    return $key . " LIKE '%". $val . "%'";
  else
    return $key . $condition . "'". $val . "'";
} // function


// Возращает строку (с формате SQL) из $array (в формате $meta_array)
function serialize_array_sql ( $array, $prefix = '' ) {
  if ( is_array( $array ) ) {
    if ( $key = is_meta_condition( $array ) ) {
      $str = "";
      foreach ( $array as $key => $val ) {
        $val = str_replace( '\'', '\\\'', $val );
        $str .= prepare_condition( $prefix, $key, $val ) . ' AND ';
      } // foreach
      $str .= "1";
      return $str;
    }; // if

    $str = '';
    foreach ( $array as $key => $val ) {
      if ( $prefix )
        $val = serialize_array_sql( $val, $prefix . '.' . $key );
      else
        $val = serialize_array_sql( $val, $key );
      if ( $val ) {
        if ( $str )
          $str .= ' AND ';
        $str .= $val;
      }; // if
    }; // foreach
    return $str;
  } else
    return $prefix . '=' . "'". str_replace( '\'', '\\\'', $array ) . "'";
} // function


// Возращает строку (с формате GET) из $array (в формате $meta_array)
function serialize_array_get ( $array, $prefix = '' ) {
  if ( is_array( $array ) ) {
    $str = '';
    foreach ( $array as $key => $val ) {
      if ( $prefix )
        $val = serialize_array_get( $val, $prefix . '[' . $key . ']' );   // !!!
      else
        $val = serialize_array_get( $val, $key );
      if ( $val ) {
        if ( $str )
          $str .= '&';
        $str .= $val;
      }; // if
    }; // foreach
    return $str;
  } else
    return $prefix . '=' . htmlspecialchars( $array );
} // function


// Функция удаляее ключи, помеченные как удалённые
function check_deleted_items ( $array ) {
  foreach ( $array as $key => $value ) {
    if ( is_array( $value ) ) {
      $array[$key] = check_deleted_items( $value );
      if ( sizeof($array[$key]) == 0 )
        unset( $array[$key] );
    } elseif ( $value == '__reset' )
      unset( $array[$key] );
  } // foreach
  return $array;
} // function


class MetaArray {
  var $classname = 'MetaArray';     // Имя класса
  var $ver = '1.3';                 // Версия
  var $meta_array = array();    // Основной массив для хранения выражений


  // Конструктор
  // В качестве аргумента $get_array используйте $_GET
  // $needed_branch - какие ветку/ветки надо извлекать из $get_array
  // если $needed_branch не задан, добавятся все ветки из $get_array
  // также добавится массив $append_array (если задан - добавлено в 1.3)
  function MetaArray ( $get_array, $needed_branches = '', $append_array = '' ) {
    if ( $needed_branches and !is_array( $needed_branches ) )
      $needed_branches = explode( ',', $needed_branches );

    // Удаляем из массива ключи, помеченные как удалённые
    $get_array = check_deleted_items( $get_array );

    foreach ( $get_array as $key => $value )
      if ( !$needed_branches or in_array( $key, $needed_branches ) )
        // Замена слешей происходит у имени файла
        $this->meta_array[$key] = str_replace( "\\\\\\\\", "\\\\", $value);

    if ( $append_array )
      foreach ( $append_array as $key => $value )
        $this->meta_array[$key] = str_replace( "\\\\\\\\", "\\\\", $value);
  } // function


  // Возращает True, если $meta_array пустой
  function is_empty ( ) {
    return $this->meta_array ? False : True;
  } // function


  // Возращает заданную ветку или ключ в ветке в $meta_array
  // $key - добавлено в 1.2
  function get ( $branch = '', $key = '' ) {
    if ( $key )
      return isset( $this->meta_array[$branch][$key] ) ?
             $this->meta_array[$branch][$key] : '';

    if ( $branch )
      return isset( $this->meta_array[$branch] ) ?
             $this->meta_array[$branch] : array();

    return $this->meta_array;
  } // function


  // Возращает строку (с формате SQL) из заданной ветки в $meta_array
  function serialize_sql ( $branches = '', $table_prefix = '' ) {
    if ( !is_array( $branches ) )
      $branches = explode( ',', $branches );
    $str = '';
    foreach ( $branches as $branch ) {
      if ( $table_prefix )
        $str1 = serialize_array_sql( MetaArray::get( $branch ), $table_prefix . '_' . $branch );
      else
        $str1 = serialize_array_sql( MetaArray::get( $branch ), $branch );
      if ( $str1 ) {
        if ( $str )
          $str .= ' AND ';
        $str .= $str1;
      }; // if
    }; // foreach
    return $str;
  } // function


  // Возращает строку (с формате GET) из заданной ветки в $meta_array
  function serialize_get ( $branches = '' ) {
    if ( !is_array( $branches ) )
      $branches = explode( ',', $branches );
    $str = '';
    foreach ( $branches as $branch ) {
      $str1 = serialize_array_get( MetaArray::get( $branch ), $branch );
      if ( $str1 ) {
        if ( $str )
          $str .= '&';
        $str .= $str1;
      }; // if
    }; // if
    return $str;
  } // function


//   // Возращает префикс для заданного имени таблицы для GET
//   function FORM_value_convert ( $keyword ) {
//     switch ( $keyword ) {
//       case '_yearago':
//         return time() - 29030400;
//       case '_6monthago':
//         return time() - 2678400 * 6;
//       case '_3monthago':
//         return time() - 2678400 * 3;
//       case '_monthago':
//         return time() - 2678400;
//       case '_weekago':
//         return time() - 604800;
//       default:
//         echo "$keyword - игнорируем<br />\n";
//         return '';
//     }; // switch
//   } // function
} // class
?>
