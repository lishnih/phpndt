<?php // Stan April 6, 2007
      //      February 17, 2009

/**
* @package      phpNDT
* @name         SqlMetaArray
* @version      1.2
* @date         24.Jan.2010
*
* @author       Stan
* @email        lishnih@gmail.com
* @copyright    2006—2010
*/

// Класс SqlMetaArray
// Данный класс является расширением класса MetaArray и предназначен
// для обработки $meta_array и создания/выполнения из него SQL-запросов


define( 'SQL_QUERY_SELECT_LIMIT', 4000 );

require 'meta_array.php';


class SqlMetaArray extends MetaArray {
  var $classname = 'SqlMetaArray';  // Имя класса
  var $ver = '1.2';                 // Версия
  var $log = '';                    // Сохраняем логи выполняемых запросов SQL
  var $db_conf = array();           // Описание базы данных
  var $cash = array();              // Вычисляемые ветки будем кешировать


  // Конструктор
  function SqlMetaArray ( $form_array, $db_conf, $append_array = '' ) {
    MetaArray::MetaArray( $form_array, $db_conf['alltables'], $append_array );
    $this->db_conf = $db_conf;
    require 'db_driver/' . $db_conf['type'] . '.php';
    $this->conn = sql_open_db( $db_conf['host'], $db_conf['user'], $db_conf['passwd'], $db_conf['name'] );
    unset( $this->db_conf['passwd'] );  // скрываем пароль
  } // function


  // Возращает выражение для запроса связь между таблицами
  function sql_relations ( $tables ) {
    if ( !is_array( $tables ) )
      $tables = explode( ',', $tables );
    $str = '';
    if ( count( $tables ) > 1 ) {
      // Если связь для таких таблиц уже высчитывали, берём из кеша
      sort( $tables );
      $tables_str = implode( ',', $tables );
      if ( isset( $this->cash[$tables_str] ) )
        return $this->cash[$tables_str];
      $this->cash[$tables_str] = '';
      $cash =& $this->cash[$tables_str];    // Адрес новой ячейки в кеше

      $db_conf =& $this->db_conf;
      $itables = array();

      // Перебираем таблицы с индексами
      for ( $i = 0; $i < count( $tables ); $i++ ) {
        $itable = $tables[$i];
        if ( isset( $db_conf[$itable]['key'] ) ) {
          $ikey = $db_conf[$itable]['key'];
          $itables[$i] = 0;    // Учитываем ссылки на эту таблицу

          // Перебираем с целью поиска ссылочных таблиц
          for ( $j = 0; $j < count( $tables ); $j++ ) {
            if ( $i == $j )
              continue;
            $table = $tables[$j];
            if ( isset( $db_conf[$itable]['relative_keys'][$table] ) ) {
              $key = $db_conf[$itable]['relative_keys'][$table];
              $itables[$i] = 1;
              $str .= " AND $itable.$ikey=$table.$key";
            }; // if
          }; // for
        }; // if
      }; // for

      // Проверяем, чтобы все требуемые таблицы были связаны
      foreach ( $itables as $val )
        if ( !$val ) {
          user_error( '+Не все связи между таблицами определены, глушим!', WARNING );
          print_ra( $tables );
          $str = ' AND 0';
        }; // if
    }; // if
    $cash = $str;
    return $str;
  } // function


  // Возращает строку SQL-запрос SELECT
  // $what   - что выбирать
  // $tables - из каких таблиц
  // $append - дополнительное условие
  // $sort   - порядок сортировки
  function sql_query_select ( $what, $tables, $append = '', $sort = '' ) {
    $what_str   = is_array( $what   ) ? implode( ',',     $what   ) : $what;
    $tables_str = is_array( $tables ) ? implode( ',',     $tables ) : $tables;
    $append_str = is_array( $append ) ? implode( ' AND ', $append ) : $append;
    $sort_str   = is_array( $sort   ) ? implode( ',',     $sort   ) : $sort;
    $where_str  = SqlMetaArray::serialize_sql( $tables );
    $where_str2 = SqlMetaArray::sql_relations( $tables );
    if ( !$where_str )
      $where_str = '1';
    if ( $where_str2 )
      $where_str .= $where_str2;
    if ( $append_str )
      $append_str = 'AND ' . $append_str;
    if ( $sort_str )
      $sort_str = 'ORDER BY ' . $sort_str;
    $query = "SELECT $what_str FROM $tables_str WHERE $where_str $append_str $sort_str";
    return $query;
  } // function


  // Выполняет SQL-запрос SELECT COUNT(*) и возращает число
  // $tables - из каких таблиц
  // $append - дополнительное условие
  function sql_select_count ( $tables, $append = '' ) {
    $query = SqlMetaArray::sql_query_select( 'COUNT(*)', $tables, $append ) . ' LIMIT 0,1';
    $this->log .= '[count] ' . $query . "\n";
    $result = sql_query( $query );
    $row = sql_fetch_row( $result );
    return $row[0];
  } // function


  // Выполняет SQL-запрос SELECT
  // $query - sql-запрос
  // $limit - максимальное количество возращаемых значений
  function sql_plain_select ( $query, $limit = SQL_QUERY_SELECT_LIMIT, $tables = null ) {
    $rows = array();
    if ( $limit )
      $query .= ' LIMIT 0,' . $limit;
    $this->log .= $query . "\n";
    $result = sql_query( $query );
    if ( !$result )
      user_error( $query );
    while ( $row = sql_fetch_array( $result ) )
      $rows[] = $row;
    if ( $limit and count( $rows ) == $limit ) {
      $count = $tables ? SqlMetaArray::sql_select_count( $tables ) : -1;
      user_error( "+Limit в SQL-запросе достигнут: $count!" );
    } // if
    return $rows;
  } // function


  // Выполняет SQL-запрос SELECT с дополнительными условиями 
  // $what   - что выбирать
  // $tables - из каких таблиц
  // $append - дополнительное условие
  // $sort   - порядок сортировки
  // $limit - максимальное количество возращаемых значений
  function sql_append_select ( $what, $tables, $append, $sort = '', $limit = SQL_QUERY_SELECT_LIMIT ) {
    $query = SqlMetaArray::sql_query_select( $what, $tables, $append, $sort );
    return SqlMetaArray::sql_plain_select( $query, $limit, $tables );
  } // function


  // Выполняет SQL-запрос SELECT
  // $what   - что выбирать
  // $tables - из каких таблиц
  // $sort   - порядок сортировки
  // $limit - максимальное количество возращаемых значений
  function sql_select ( $what, $tables, $sort = '', $limit = SQL_QUERY_SELECT_LIMIT ) {
    return SqlMetaArray::sql_append_select( $what, $tables, '', $sort, $limit );
  } // function


//   function sql_joint_select ( $what, $tables, $dict_name, $dict, $limit = SQL_QUERY_SELECT_LIMIT ) {
//     $append_str = '';
//     foreach ( $dict as $key->$value )
//       $append_str = " AND $dict_name.$key='$value'";
//     return SqlMetaArray::sql_append_select( $what, $tables, $append_str, $sort, $limit );
//   } // function


  // Выполняет SQL-запрос SELECT и возращает один ряд
  // $what   - что выбирать
  // $tables - из каких таблиц
  // $sort   - порядок сортировки
//   function sql_select_row ( $what, $tables, $sort = '' ) {
//     $result = SqlMetaArray::sql_select( $what, $tables, $sort );
//     $row = sql_fetch_row( $result );
//     return $row[0];
//   } // function
} // class
?>
