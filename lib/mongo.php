<?php // Stan 2011-09-21

function sprint_mongo ( $row ) {
  global $collection;
  $kind   = $row['joint_kind'];
  $type   = $row['joint_pre'];
  $number = $row['joint_seq'];
  $sign   = $row['joint_sign'];
//   $kp     = $row['kp'];
//   $signE  = $row['signE'];

  if ( $kind == 10 ) {          // Линейные
    $find = array(
                  'kp'     => 'C2',
                  'type'   => $type,
                  'number' => array( '$in' => array( (string) $row['joint_seq'], (int) $row['joint_seq'], (float) $row['joint_seq'] ) )
                 );
  } // if

  $str = '';

  $cursor = $collection->find( $find );

//   $sort = array( '_id' => 1 );
//   try {
//     $cursor = $cursor -> sort( $sort );
//   } catch( Exception $e ) {
//     $str = '?';
//     $cursor = False;
//   } // try
//   $cursor = $cursor -> limit( 3 );

  if ( $cursor ) {
    $count = $cursor->count();
  
    foreach( $cursor as $key => $row ) {
      $joint_str = $row['kp'] . '/' . $row['type'] . '/' . $row['number'];
      $scheme_str = $row['scheme'];
      $delm = '     ';
      $title_str = 'date: '    . $row['date'] . $delm .
                   'dt1: '     . $row['d1'] . 'x' . $row['t1'] . $delm .
                   'dt2: '     . $row['d2'] . 'x' . $row['t2'] . $delm .
                   'scheme: '  . $row['scheme'] . $delm .
                   'welders: ' . $row['welders'] . $delm .
                   'VT: '      . $row['VT'] . $delm .
                   $row['RT_is'] . ': ' . $row['RT'] . '[' . $row['RT_des'] . ']' . $delm .
                   'VT_r: '    . $row['VT_r'] . $delm .
                   'RT_r: '    . $row['RT_r'] . '[' . $row['RT_rdes'] . ']' . $delm .
                   'UT: '      . $row['UT'] . $delm .
                   '('         . $row['id_old'] . ')';
      $str .= '<span title="' . $title_str . '">' . $joint_str . '</span>';
      $str .= ' <a href="extras/register_isp_j.php?scheme=' . $scheme_str . '">.</a>';
      $str .= '<br />';
    } // foreach
  } else
    $str = '-';

  return $str;
} // function

?>
