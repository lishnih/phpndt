<?php // Stan 23 ноября 2006г.
// Функции отображения данных различных типов из таблицы


// Преобразует дюймы (внутреннее представление) в мм
function inch2mm ( $inch ) {
  return (int)( $inch * 25.4 );
} // function


// Задать цвет тексту
function sprint_color ( $str, $color, $bgcolor = '', $title = '' ) {
  if ( $bgcolor )
    $bgcolor = "; background-color: $bgcolor";
  if ( $title )
    $title = 'title="' . $title . '"';
  return "<span $title style=\"color: $color; font-weight: bold$bgcolor\">$str</span>";
} // function


// Задать цвет тексту
function sprint_inactive ( $str ) {
  return '<span style="background-color: gray">' . $str . '</span>';
} // function


// форматирует и возращает дату
// параметры: дата в числовом представлении
function sprint_date ( $date ) {
  if ( !$date )
    return '';
  if ( is_array( $date ) ) {
    $date = $date['date_str'];
    return $date ? $date : '<i>none</i>';

    $date = $date['date'];
    return $date ? date( 'd.m.Y', $date ) : '<i>none</i>';
  } // if
  if ( !is_numeric( $date ) )
    return '<i>? ' . $date . ' ?</i>';
  return date( 'd.m.Y', $date );
} // function


// форматирует и возращает Решение стыка
// параметр - Решение стыка
function sprint_decision ( $decision, $form = 0 ) {
  switch( $decision ) {
// Неопределено
    case '':        return sprint_color( '[]', 'red' );

// Принят
    case 'ГОДЕН':
    case 'Годен':
    case 'годен':   return sprint_color( $decision, 'green' );
// Ремонт
    case 'РЕМОНТ':
    case 'Ремонт':
    case 'ремонт':  return sprint_color( $decision, 'red' );

// Неизвестное
    default:        return sprint_color( '&nbsp;' . $decision . '&nbsp;', 'Crimson', 'black' );
  }; // switch
} // function


// форматирует и возращает обозначение отчёта
function get_name ( $a, $key = '' ) {
  $key = $key ? $key : 'name';
  return $a[$key];
} // function


// возращает ссылку на отчёт
function link_report ( $report, $key = '', $id = '', $str = '' ) {
  $name = $str ? $str : get_name( $report, $key );
  $id = $id ? $id : 'id';
  $link_str = 'report.php?reports[id]=' . $report[$id];
  return '<a href="' . $link_str . '">' . $name . '</a>';
} // function


// возращает ссылку на отчёт
function link_scan_report ( $report, $key = '', $str = '' ) {
  $name = $str ? $str : get_name( $report, $key );

  $filename = '%s/%s/%s-%04d.jpg';
  $path = 'http://localhost/pgu235/scan/Заключения';
  $method = $report['method'];
  $link_str = sprintf( $filename, $path, $method, $method, $report['report_seq'] );
  return '<a href="' . $link_str . '">' . $name . '</a>';
} // function


// форматирует и возращает пикет
// function sprint_kp ( $report ) {
//   $kp   = $report['kp'];
//   $site = $report['site'];
//   if ( $site )
//     return $kp . ' / ' . $site;
//   else
//     return $kp;
// } // function


// форматирует и возращает диаметр и толщину
// параметры: диаметр толщина и требуемый стандарт (API или ВСН)
function sprint_dt ( $joint ) {
  return sprint_diameter( $joint ) . 'x' . sprint_thickness( $joint );
} // function


// форматирует и возращает диаметр
function sprint_diameter ( $joint ) {
  if ( isset( $joint['diameter'] ) )
    return $joint['diameter'];

  $diameter1 = $joint['diameter1'];
  $diameter2 = $joint['diameter2'];
  if ( $diameter2 )
    return $diameter1 . '/' . $diameter2;
  else
    return $diameter1;
} // function


// форматирует и возращает толщину
function sprint_thickness ( $joint ) {
  if ( isset( $joint['thickness'] ) )
    return $joint['thickness'];

  $thickness1 = $joint['thickness1'];
  $thickness2 = $joint['thickness2'];
  if ( $thickness2 )
    return $thickness1 . 'x' . $thickness2;
  else
    return $thickness1;
} // function


// возращает ссылку на стык
function link_joint ( $report, $key = '', $id = '', $str = '' ) {
  $name = $str ? $str : get_name( $report, $key );
  $id = $id ? $id : 'id';
  $link_str = 'joint.php?joints[id]=' . $report[$id];
  return '<a href="' . $link_str . '">' . $name . '</a>';
} // function


// Работа со стыками


// возращает наибольшую толщину
function maxthickness ( $joint ) {
  $thickness  = $joint['thickness'];
  $thickness2 = $joint['thickness2'];
  return $thickness2 > $thickness ? $thickness2 : $thickness;
} // function


function is_repair ( $joint ) {
  if ( substr( $joint['sign2'], -1 ) == 'R' )
    return True;
  else
    return False;
} // function


function is_tie ( $joint ) {
  if ( substr( $joint['sign'], -1 ) == 'T' OR substr( $joint['sign'], 1, 1 ) == 'T' )
    return True;
  else
    return False;
} // function


function is_lam ( $joint ) {
  if ( $joint['kind'] == -1 )
    return True;
  else
    return False;
} // function
?>
