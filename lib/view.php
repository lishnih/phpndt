<?php // Stan 06 апреля 2007г.

$append_array = isset( $_GET['kp'] ) ? array( 'joints'=> array( 'kp'=> $_GET['kp'] ) ) : '';

$meta_get = new SqlMetaArray( $_GET, $db_conf, $append_array );

$get_str = $meta_get->serialize_get();
$db_name = $db_conf['name'];
$alltables = $db_conf['alltables'];


// Складывает GET-строки
function sum_gets ( $get_str1, $get_str2 ) {
  if ( $get_str1 and $get_str2 )
    $get_str1 .= '&';
  return $get_str1 . $get_str2;
} // function


function view_header_all ( ) {
global $meta_get, $db_name;
  $result = $meta_get->sql_plain_select( "SHOW TABLES FROM `$db_name`", 0 );
//   $result = $meta_get->sql_plain_select( "SELECT name FROM sqlite_master", 0 );
  if ( !$result ) {
    $error_str = mysql_error();
    if ( $error_str ) {
      print "DB Error, could not list tables!\n";
      print 'MySQL Error: ' . mysql_error();
      exit;
    } else {
      print "No tables!\n";
      exit;
    }; // if
  } // if

  foreach ( $result as $table ) {
    $row = array_values( $table );
    $tablename = $row[0];

    if ( $tablename == 'tasks' or $tablename == 'dirs' or
         $tablename == 'files' or $tablename == 'sheets' )
      continue;

    $count = $meta_get->sql_select_count( $tablename );
    echo <<<EOD
<fieldset><legend>$db_name/<a href="?show_table=$tablename">$tablename</a> ($count)</legend>
<table border="1">\n
EOD;
    $fields = mysql_list_fields( $db_name, $tablename, $meta_get->conn );
    $columns = mysql_num_fields( $fields );
    for ( $i = 0; $i < $columns; $i++ ) {
      $fieldname = mysql_field_name( $fields, $i );
      if ( $fieldname != 'id' and $fieldname != 'name' and substr( $fieldname, 0, 1 ) != '_' ) {
        $max = ( $fieldname == 'name' or $fieldname == 'date' or
                 $fieldname == 'date_str' ) ? 10 : 100;
        $br  = ( $fieldname == 'dirname' or $fieldname == 'filename' ) ? 1 : 0;
        $operand = ( $fieldname == 'thickness1-!!!' or $fieldname == 'thickness2-!!!' ) ? '~' : '';      
        list_selects( $tablename, $fieldname, '', $operand, $max, $br );
      }; // if
    } // for
    echo "</table></fieldset><br />\n";
  } // foreach
}


// Перечень значений для заданного поля $field в таблице $tablename
// Возможно задания условия $operand вместо равенства
function list_selects ( $tablename, $field, $str = '', $operand = '', $max = 100, $br = 0 ) {
global $meta_get, $alltables, $options, $get_str;
  if ( array_search( 'linked', $options ) !== False )
    $rows = $meta_get->sql_select( "DISTINCTROW $tablename.$field", $alltables, $field, 0 );
  else
    $rows = $meta_get->sql_select( "DISTINCTROW $field", $tablename, $field, 0 );
  $val = isset( $meta_get->meta_array[$tablename][$field] ) ?
         $meta_get->meta_array[$tablename][$field] : False;

  if ( is_array( $val ) ) {
    $val_is_condition = True;
    $condition = key( $val );
  } else
    $val_is_condition = False;

  echo '<tr><td>' . ( $str ? $str : $field ) . '<td>';

  if ( $rows ) {
    $count = count( $rows );
    echo $count . "<td>\n";
    $icount = $count > $max ? $max : $count;
    // В 3ью колонку выводим все значения по очереди
    for( $i = 0; $i < $icount; $i++ ) {
      $row = $rows[$i][$field];
      $htmlscrow = htmlspecialchars( $row );
      // Если это значение совпадает со значением из GET, то выделаем его
      if ( $val === $row ) {
        echo '<b>' . ( $row ? $htmlscrow : '<i>пусто</i>' ) . '</b> ';
        // также предлагаем его сбросить и инвертировать
        if ( $val !== False )
          echo '<a href="?' . sum_gets( $get_str, $tablename ) . '[' . $field . ']=__reset">(сбросить)</a> / ' .
               'операции: ' .
               '<a href="?' . sum_gets( $get_str, $tablename ) . '[' . $field . '][<]='  . $val . '">&lt;</a> ' .
               '<a href="?' . sum_gets( $get_str, $tablename ) . '[' . $field . '][<<]=' . $val . '">&lt;=</a> ' .
               '<a href="?' . sum_gets( $get_str, $tablename ) . '[' . $field . '][>]='  . $val . '">&gt;</a> ' .
               '<a href="?' . sum_gets( $get_str, $tablename ) . '[' . $field . '][>>]=' . $val . '">&gt;=</a> ' .
               '<a href="?' . sum_gets( $get_str, $tablename ) . '[' . $field . '][!]='  . $val . '">!=</a> ' .
               '<a href="?' . sum_gets( $get_str, $tablename ) . '[' . $field . '][%]='  . $val . '">%</a> ';
      } else {
        $expression = $operand ? $tablename . '[' . $field . '][' . $operand . ',' . $htmlscrow . ']' :
                                 $tablename . '[' . $field . ']=' . $htmlscrow;
        echo '<a href="?' . sum_gets( $get_str, $expression ) . '">[' . $htmlscrow . ']</a> ';
        if ( $br )
          echo "<br />";
      }; // if
    }; // foreach
    if ( $count > $icount )
      echo '<a href=""><b>...</b></a>';
    if ( $val_is_condition )
      echo '<a href="?' . sum_gets( $get_str, $tablename ) . '[' . $field . ']=__reset">(сбросить)</a>';
  } elseif ( $val !== False ) {
    if ( is_array( $val ) )
      echo '<td><i>Значение не найдено!';
    else
      echo '<td><i>Значение не найдено! Попробуйте ' .
           '<a href="?' . sum_gets( $get_str, $tablename ) . '[' . $field . '][~]='  . $val . '">изменить условие</a></i>';
  } else
    echo '<td><i>пусто</i>';
  echo "\n";
} // function


// Выводит начало таблицы
function start_table ( ) {
global $started_tables;
  $table_options = array();
  $table_options['dont_close_thead'] = False;

  $table_id = count( $started_tables ) + 1;
  $this_table =& $started_tables[$table_id];

  echo '<table cellpadding="0" cellspacing="1" border="1" id="table' . $table_id . '" class="tablesorter">';
  $ncols = func_num_args();
  if ( $ncols )
    echo "\n <thead>\n  <tr>\n";
  for ( $i = 0; $i < $ncols; $i++ ) {
    $arg = func_get_arg( $i );
    if ( is_array( $arg ) ) {
      foreach( $arg as $key => $row )
        if ( $key )
          switch ( $key ) {
            case 'id':
              $this_table['id'] = $row;
              $this_table[$i][$key] = $row;
              break;
            case '!dont_close_thead':
              $table_options['dont_close_thead'] = $row;
              break;
            default:
              $this_table[$i][$key] = $row;
          } // switch
        else
          $str = $row;
    } else
      $str = $arg;
    echo "   <th>$str</th>\n";
  }; // for
  if ( !$table_options['dont_close_thead'] )
    echo "  </tr>\n </thead>\n <tbody>\n";
  return $ncols;
} // function


// Выводит ряд таблицы
function echo_tr ( ) {
global $started_tables;
  $table_id = count( $started_tables );
  $this_table =& $started_tables[$table_id];
  echo '  <tr>';
  for ( $i = 0; $i < func_num_args(); $i++ ) {
    if ( isset( $this_table[$i]['id'] ) ) {
      $d = func_get_arg( $i );
      echo '<td align=center>';
      if ( $d ) {
        echo $this_table['id'];
        $this_table['id'] += $d;
      }; // if
      echo '</td>';
      continue;
    } // if
    if ( isset( $this_table[$i]['td'] ) )
      echo '<td ' . $this_table[$i]['td'] . '>' . func_get_arg( $i );
    else
      echo '<td>' . func_get_arg( $i );
    echo '</td>';
  } // for
  echo "\n  </tr>\n";
} // function


// Выводит конец таблицы
function stop_table ( ) {
global $started_tables;
  echo " </tbody>\n</table>\n";
  $table_id = count( $started_tables );
//   print_ra( $started_tables );
  unset( $started_tables[$table_id] );
} // function


// Выводит группированный список из MySQL результата
// просто задумка - не работает !!!!!!!!
function group_table ( $result, $groupby, $header_list ) {
  $group = '';
  while ( $row = mysql_fetch_array( $result, MYSQL_ASSOC ) ) {
//  print_rt( $row );
    // Если началась новая группа, то закрываем предыдущую
    if ( $group != $groupby ) {
      stop_table();
      echo "<br />\n";
    }; // if

    // Если ещё не было групп или началась новая группа, то выводим новую группу
    if ( !$group AND $group != $groupby ) {
      $group = $groupby;
      echo "<H3>" . $group . "</H3>\n";
//       start_table( list( $header_list ) );
    }; // if

    // Выводим ряд
    echo_tr( $row['kind'], sprint_report( $row ), sprint_diameter( $row ), sprint_kp( $row ),
             sprint_date( $row['date'] ), $row['joints'] );
    echo '<td>';
    $result2 = sql_joints_of_report( $row, $method );

  }; // while
  stop_table();
} // function

function print_debug ( ) {
global $get_str, $meta_get, $mode, $options;
  echo '$_GET, $get_str, $mode, $options, $meta_get:' . "<br />\n";
  print_rb( $_GET, $get_str, $mode, $options, $meta_get );
}
?>
