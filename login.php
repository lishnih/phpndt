<?php // Stan 5 марта 2007г.
include '_local.php';

// Принудительный вход пользователя, если ещё не аутентифицировался
if ( $user === False )
  auth();
else {
  $home  = isset( $_GET['home']  ) ? $_GET['home']  : '';
  $enter = isset( $_GET['enter'] ) ? $_GET['enter'] : '';
  $exit  = isset( $_GET['exit']  ) ? 1 : 0;   // зарезервировано
  $REQUEST_URI = $_SERVER['REQUEST_URI'];

  // Повторный вход пользователя (по требованию пользователя)
  if ( $enter ) {
    if ( $enter == $user[0] )
      auth();
    else {
      $pos = strrpos( $REQUEST_URI, '?' );
      if ( $pos !== False ) 
        $home = substr( $REQUEST_URI, 0, $pos );
    }; // if

  // Если пользователь аутентифицировался, то направить его на домашнюю страницу
  } else {
    if ( !$home ) {
      $pos = strrpos( $REQUEST_URI, '/' );
      if ( $pos !== False ) 
        $home = substr( $REQUEST_URI, 0, $pos + 1 );
    }; // if
  }; // if

  if ( $home )
    header( "Location: $home" );
}; // if

echo 'Пользователь не аутентифицировался!';
$bgcolor = $bgerrorcolor;
?>
