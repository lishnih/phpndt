<?php	// Stan 21 сентября 2006г.

// Все классы NDT должны быть наследованы от этого класса
class ndtParentClass {
  function ndtParentClass ( $str ) {
    $this->Name    = 'ndtParentClass';
    $this->Str     = $str;
    $this->db_code = 0;		// режим записи в базу данных
    $this->db_str  = '';	// разъяснение запрета записи

// Эти массивы используются для trace() и распознавания
// Массив для переменных которые встречаются один раз в документе
    $this->search      = array();
    $this->replace     = array();
// Массив для переменных которые встречаются несколько раз в документе
    $this->search_all  = array();
    $this->replace_all = array();
    return 1;
  } // Конструктор ndtClass

////////////////////////////
//   Отладочные функции   //
////////////////////////////

  function trace ( ) {
    $str = preg_replace( $this->search,     $this->replace,     $this->Str );
    $str = preg_replace( $this->search_all, $this->replace_all, $str );
    echo "\n<br />Трассировка: <b>$this->Name</b>\n";
    print_ra( $str );
    return 1;
  } // function

  function error ( $str, $code = E_USER_NOTICE ) {
    $name = "[ $this->Name ] ";
    user_error( $name . $str, $code );		// Выводим на экран
    return True;
  } // function

//////////////////////////////
//   Интерфейсные функции   //
//////////////////////////////

// Выполнить запрос в базу данных
  function query ( $str, $fault_str = '' ) {
    if ( !$result = @mysql_query( $str ) ) {
      user_log( $fault_str ? $fault_str : $str, ERROR );	// Заносим в лог
      user_error( "<b>$fault_str</b><br />\n(" . mysql_errno() . ') ' . mysql_error() . "<br />\n" . $str, ERROR );
      return False;
    }; // if
    return $result;
  } // function

// Преобразует многострочную ячейку в однострочную
  function simplify ( $str ) {
    if ( preg_match_all( '/"(.*)"/s', $str, $matches ) ) {
      $str = $matches[1][0];
      $str = str_replace( "\n", ' ', $str );
      $str = str_replace( "\r", '', $str );
    }; // if
    return $str;
  } // function

// Убирает пробелы в строке и переводит в верхний регистр
  function trim ( $str ) {
    $str = trim( strtoupper( $str ) );
    $str = preg_replace( '/ */', '', $str );
    return $str;
  } // function

// Убирает лишние пробелы в строке и переводит в верхний регистр
  function trim2 ( $str ) {
    $str = trim( strtoupper( $str ) );
    $str = preg_replace( '/ {2,}/', ' ', $str );
    return $str;
  } // function

/////////////////////////////////
//   Вспомогательные функции   //
/////////////////////////////////

// Преобразует строку даты в числовое выражение
  function date_str_to_int ( $str ) {
    // Строка в формате ДД .* ММ .* ГГ | ГГГГ
    $str = $this->trim( $str );
    if ( !$str ) {
      $this->error( 'Дата не представлена!' );
      return 0;
    }; // if
    if ( preg_match( '/^(\d{1,2}).?(\d{1,2}).?(\d{2,4})/', $str, $matches ) ) {
      return mktime( 20, 0, 0, $matches[2], $matches[1], $matches[3] );
    } else {
      $this->error( "Дата не распознана: $str", WARNING );
      return False;
    }; // if
  } // function

// Преобразует строку даты в числовое выражение
  function split_diameter ( $str ) {
    // Диаметр и толщина в формате DDDD x TT1 [ x TT2 ]
    $str = $this->trim2( $str );
    $str = str_replace( 'Х', 'X', $str );

    // API:              21,1              x    17,6             48         "
    if ( preg_match( '/^([0-9,.]{3,5})(?: *X? *([0-9,.]{3,4}))? ([0-9]{1,2})"$/', $str, $matches ) ) {
                                // 5 для 10,97
      $diameter = (int) $matches[3];
      $thickness1 = (float) str_replace( ',', '.', $matches[1] );
      $thickness2 = isset( $matches[2] ) ? (float) str_replace( ',', '.', $matches[2] ) : 0;
    // ВСН:                    1219         21,1                  17,6
    } elseif ( preg_match( '/^([0-9]{2,4}) ([0-9,.]{1,4})(?: *X *([0-9,.]{1,4}))?$/', $str, $matches ) ) {
      $diameter = (int) $matches[1];
      $thickness1 = (float) str_replace( ',', '.', $matches[2] );
      $thickness2 = isset( $matches[3] ) ? (float) str_replace( ',', '.', $matches[3] ) : 0;
    } else {
      $this->error( "Диаметр и толщина не распознаны: $str", WARNING );
      return array( 0, 0 );
    }; // if
    if ( !$thickness2 OR $thickness2 > $thickness1 )
      $thickness = $thickness2 * 1000 + $thickness1;
    else
      $thickness = $thickness1 * 1000 + $thickness2;
    return array( $diameter, $thickness );
  } // function

// Преобразует обозначение отчёта во внутреннее представление
// $str      - обозначение отчёта
//  function decompound_report ( $str ) {
//
//  } // function


// Преобразует обозначение стыка во внутреннее представление
// $str      - обозначение стыка
// $id       - запрос на поиск стыка в базе данных
// Описание kind - тип стыка
//  1 - S2
//  2 - стандартное обозначение
//  4 - аттестация
//  8 - без км
// 16 - Test Header, краны и т.д.
  function decompound_butt_id ( $str, $id = 0 ) {
    $str = ndtParentClass :: trim2( $str );	// для view weld

    $kind = 0;
    $pos = strpos( $str, 'S2' );
    if ( $pos === 0 )
      $kind = 1;
// ---------------------------------------------------------
//                       1              2          3                4      5
//                       $pre           $kp        $type            $butt  $sign
// S2BG255RVX1-01MR2     S2BG           255        RVX1             01     MR2
    if ( preg_match( '/^([A-Z0-9 ]*?) *(\d{2,3}) *([A-Z0-9 ]*) *- *(\d*) *([A-Z0-9 ]*)$/', $str, $matches ) ) {
// ---------------------------------------------------------
      $pre  = ndtParentClass :: trim( $matches[1] );
      $kp   = (int)$matches[2];
      $type = ndtParentClass :: trim( $matches[3] );
      $butt = (int)$matches[4];
      $sign = ndtParentClass :: trim( $matches[5] );
      //print_ra( $matches );
      $kind |= 2;
// ---------------------------------------------------------
//                             1                 2          3                       4
//                             $pre              $kp        $type                   $sign
// S2B GAT-56 WPS FC-01
    } elseif ( preg_match( '/^([A-Z0-9 ]*?) *- *(\d{2,3}) *([A-Z0-9 ]*) *(?:WPS) *([A-Z0-9 -]*)$/', $str, $matches ) ) {
// ---------------------------------------------------------
      $pre  = ndtParentClass :: trim( $matches[1] );
      $kp   = (int)$matches[2];
      $type = ndtParentClass :: trim( $matches[3] );
      $butt = False;
      $sign = ndtParentClass :: trim( $matches[4] );
      print_ra( $matches );
      $kind |= 4;
// ---------------------------------------------------------
//                             1                 2      3
//                             $pre              $butt  $sign
    } elseif ( preg_match( '/^([A-Z0-9 ]*?) *- *(\d*) *([A-Z0-9 ]*)$/', $str, $matches ) ) {
// ---------------------------------------------------------
      $pre  = ndtParentClass :: trim( $matches[1] );
      $kp   = False;
      $type = False;
      $butt = (int)$matches[2];
      $sign = ndtParentClass :: trim( $matches[3] );
      //print_ra( $matches );
      $kind |= 8;
// ---------------------------------------------------------
//                             1              2                 3      4
//                             $pre           $type             $butt  $sign
// 036-XV-229FW
    } elseif ( preg_match( '/^([A-Z0-9 -]*?) ([FTW]{1,2})? *- *(\d*) *([A-Z0-9 ]*)$/', $str, $matches ) ) {
// ---------------------------------------------------------
      $pre  = ndtParentClass :: trim( $matches[1] );
      $kp   = False;
      $type = ndtParentClass :: trim( $matches[2] );
      $butt = (int)$matches[3];
      $sign = ndtParentClass :: trim( $matches[4] );
      //print_ra( $matches );
      $kind |= 16;
// ---------------------------------------------------------
//                       1              2          3                4      5
//                       $pre           $kp        $type            $butt  $sign
// Test Headers
    } elseif ( preg_match( '/^([A-Z0-9 ]*?) *(\d{2,3}) *([A-Z0-9 \/-]*) *- *(\d*) *([A-Z0-9 ]*)$/', $str, $matches ) ) {
// ---------------------------------------------------------
      $pre  = ndtParentClass :: trim( $matches[1] );
      $kp   = (int)$matches[2];
      $type = ndtParentClass :: trim( $matches[3] );
      $butt = (int)$matches[4];
      $sign = ndtParentClass :: trim( $matches[5] );
      //print_ra( $matches );
      $kind |= 32;
// ---------------------------------------------------------
    } else {
// ---------------------------------------------------------
      $this->error( "Стык не соответствует шаблону: $str!", WARNING );
      return 0;
    }; // if

    $idw = 0;
    // Ищем этот стык в butts если необходимо
    if ( $id ) {
      $result = ndtParentClass :: query( "SELECT `id` FROM `butts` WHERE `kp`='$kp' AND `butt`='$butt' AND `sign`='$sign' AND `type`='$type'" );
      $id = mysql_fetch_row( $result );
      $id = $id[0] ? $id[0] : -1;
  //  $result = ndtParentClass :: query( "SELECT `id` FROM `welds` WHERE `kp`='$kp' AND `butt`='$butt' AND `sign`='$sign' AND `type`='$type'" );
  //  $idw = mysql_fetch_row( $result );
  //  $idw = $idw[0] ? $idw[0] : -1;
    }; // if
    return array (	'pre'  => $pre,
			'kp'   => $kp,
			'type' => $type,
			'butt' => $butt,
			'sign' => $sign,
			'kind' => $kind,
			'id'   => $id,		// $id  - номер стыка в таблице butts
			'idw'  => $idw );	// $idw - номер стыка в таблице welds
  } // function
} // Класс ndtParentClass
?>
