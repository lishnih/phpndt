<?php // Stan 14 апреля 2007г.

define( 'SHOW_INFO', 1 );
define( 'SHOW_VSN',  2 );
define( 'SHOW_ALL',  SHOW_INFO | SHOW_VSN );

// Все классы NDT наследованы от этого класса
if ( !include_once '__ndtParentClass.php' )
  user_error( 'Объект ndtParentClass не найден!', ERROR );

class ndtApiClass extends ndtParentClass {
  function ndtApiClass ( $str ) {
    $this->ndtParentClass( $str );
    $this->Name = 'ndtApiClass';

    //////////// Эти переменные встречаются один раз в отчёте ////////////
    // номер отчёта и дата ищутся одинаково для всех типов API-отчётов
    // следует отметить, что табуляция не вырезается после обработки в recognize_api_singles()
    $this->search     ['report'] = '/Report[^\t]{1,16}[\t]+([^\t]+)\t/';
    $this->search     ['date']   = '/Date[^\t]{1,15}\t+([^\t]+)\t/';
    $this->replace    ['report'] = '<span style="color: red"><b>$0</b></span>';
    $this->replace    ['date']   = '<span style="color: green"><b>$0</b></span>';

    //////////// Эти переменные встречаются несколько раз в документе ////////////
    // данные для поиска имен в отчётах
    $this->search_all ['names']  = '/(Name[^:]+): *([A-zА-я .\/]*)(\t+) *([A-zА-я .\/]*)(\t+)/e';
    $this->replace_all['names']  = "('$4' ? '$1:$3<span style=\"color: brown\"><b>$4</b></span>$5' : '$1: <span style=\"color: yellow\"><b>$2</b></span>$3$5' )";

    return 1;
  } // Конструктор ndtClass

/////////////////////////////////////////
//   Функции распознавания отчётов API //
/////////////////////////////////////////

  function recognize_api_singles ( ) {
    // Перебираем имена необходимых переменных
    while( list( $varname, $search ) = each( $this->search ) ) {
      $this->$varname = False;
      if ( preg_match( $search, $this->Str, $matches ) ) {
        // присваиваем значение переменной с именем ключа
        $this->$varname = trim( $matches[1], " \r\n" );
        // если в патерне распозналось больше значений, то
        // присваиваим их переменным с индексом
        $c = count( $matches );
        if ( $c > 2 )
          for ( $i = 2; $i < $c; $i++ )
            $this->{$varname.$i} = trim( $matches[$i], " \r\n" );
        // проверка на значение
        if ( !$this->$varname )
          $this->error( "Переменная пуста: $varname" );
      } else
        $this->error( "Переменная не найдена: $varname", WARNING );
    }; // while

    // Преобразуем номер отчёта
    $this->kind = 0;	// 1 - есть числовой КМ
			// 2 - есть нечисловое обозначение (SOB, POB или MOB; Onor, Base и т.д.)
			// 1 + 2 - и то и другое
    $this->site = '';

    // Разбираемся в км
    if ( isset( $this->kp2 ) AND $this->kp2 ) {
      if ( is_numeric( $this->kp ) ) {
        $this->site = $this->kp2;
        $this->kind |= 2;
      } else
        $this->error( "$this->kp <> $this->kp2" );
    } elseif ( $this->kp )
      if ( is_numeric( $this->kp ) )
        $this->kind |= 1;
      else {
        $this->kp = 0;
        $this->site = $this->kp;
        $this->kind |= 2;
      }; // if

			// 4 - G отчёт
			// 8 - GAT или GT (аттестация)
    if ( $this->report )
      if ( preg_match( '/^(.*) {3,}(.*)$/', $this->report, $matches ) ) {
        $this->report = $matches[1];
      }; // if
      if ( preg_match( '/^([A-Z ]*) *-? *(\d*) *(.*)$/', $this->report, $matches ) ) {
        $this->pre    = $this->trim( $matches[1] );
        $this->report = (int)$matches[2];
        $this->sign   = $this->trim( $matches[3] );
        if ( $this->pre == 'G' )
          $this->kind |= 4;
        elseif ( $this->pre == 'GAT' OR $this->pre == 'GT' )
          $this->kind |= 8;
      } else {
        $this->error( 'Отчёт не определён: ' . $this->report, WARNING );
        $this->pre    = $this->report;
        $this->kind   = 0;
      }; // if

    // Преобразуем дату в целое число
    if ( $this->date )
      if ( !$this->date = $this->date_str_to_int( $this->date ) )
        $this->error( 'Дата не преобразована', WARNING );
    return 1;
  } // function

  function recognize_api_names ( ) {
    if ( preg_match_all( $this->search_all['names'], $this->Str, $matches ) ) {
      for ( $i = 0; $i < count( $matches[0] ); $i++ )
        if ( !$this->{'name'.$i} = $matches[4][$i] ? $matches[4][$i] : $matches[2][$i] )
          $this->error( 'Переменная не найдена: name' . $i );
    } else
      $this->error( 'Не найдено ни одно имя!', WARNING );
    return 1;
  } // function

  function trace ( ) {
    print_ra( $this );
    ndtParentClass :: trace();
    return 1;
  } // function
} // Класс ndtParentClass
?>
