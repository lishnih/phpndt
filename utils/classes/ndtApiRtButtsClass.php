<?php	// Stan 22 сентября 2006г.

// На входе:    Строки данных о стыках из рапорта API
// Результат:   Распознавание данных
//              Генерирование таблицы ВСН
// На выходе:   Таблица ВСН
// Трассировка: ---

if ( !include_once '_ndtApiClass.php' )
  user_error( 'Объект ndtApiClass не найден!', ERROR );


// Последовательность столбцов в таблице RT API
// Rev 5 с 22 сентября 2006г.
  define( 'ID',        1 );
  define( 'WELDERS',   2 );
  define( 'THICKNESS', 3 );
  define( 'SOURCE',    4 );
  define( 'DENSITY',   5 );
  define( 'VWIRE',     6 );
  define( 'DEFECTS',   7 );
  define( 'DECISION',  8 );
  define( 'REMARKS',   9 );


class ndtApiRtButtsClass extends ndtParentClass {
  function ndtApiRtButtsClass ( $str ) {
    $this->ndtParentClass( $str );
    $this->Name = basename( __FILE__, '.php' );

    $this->check = '/(\t{10}[^\t\n\r])/';
//                                    1id       2weld     3diam          4s        5dens     6wpipe    7def           8заключение 9rem
    $this->search_all['butts']  = '/^([^\t]+)\t([^\t]+)\t([^\t]+)\t{1,2}([^\t]+)\t([^\t]+)\t([^\t]+)\t([^\t]+)\t{11,16}([^\t]+)\t([^\t]*)/m';
    $this->replace_all['butts'] = '<span style="color: blue"><b>$0</b></span>';
    return 1;
  } // Конструктор ndtClass


  // Эта функция вызывается из convert.php
  function run ( ) {
    $code = $this->recognize_butts();
    $this->print_vsn_table();
    return $code;
  } // function


  function show_info ( ) {	// Показ инфы о стыках
    //print_ra( $this );
    $kp       = ( $this->kp === True  ) ? '<i>Не обозначен</i>' :
              ( ( $this->kp === False ) ? '<i>Провал проверки</i>' : $this->kp );
    $diameter = ( $this->diameter === False ) ? '<i>Провал проверки</i>' : $this->diameter;
    echo "<pre>
<b>Информация по стыкам:</b>
kp:          $kp
kind:        " . decbin( $this->kind ) . "b
diameter:    $diameter
num_butts:   $this->num_butts
</pre>";
    start_table( '', 'pre', 'kp', 'type', 'butt', 'sign', 'kind',
                     'welders', 'thickness', 'diameter', 'source', 'density',
                     'v_wire', 'status', 'remarks' );
    while ( list( $key, $v ) = each( $this->butts ) )
      if ( $v['kind'] === False )
        echo_tr( $key, $v['pre'], '-', '-', '-', '-', '<i>False</i>',
                       $v['welders'], sprint_thickness( $v['thickness'] ), $v['diameter'], $v['source'], $v['density'],
                       $v['v_wire'], sprint_decision( $v['status'] ), $v['remarks'] );
      else
        echo_tr( $key, $v['pre'], $v['kp'], $v['type'], $v['butt'], $v['sign'], $v['kind'],
                       $v['welders'], sprint_thickness( $v['thickness'] ), $v['diameter'], $v['source'], $v['density'],
                       $v['v_wire'], sprint_decision( $v['status'] ), $v['remarks'] );
    stop_table();
  } // function


  // Вызывается из run() и ndtApiRtClass::run()
  // Ничего не выводит, создаёт массив butts - распознанные параметры стыков
  function recognize_butts ( ) {
  global $debug, $kp_min, $kp_max, $status_title;
    if ( preg_match_all( $this->check, $this->Str, $matches_all ) ) {
      $num_butts = count( $matches_all[1] );
      //print_sh( $matches_all[1][1] );
      //print_rt( $matches_all );
    } else {
      $this->error( 'Не найдено ни одного стыка!', WARNING );
      return 0;
    }; // if
    if ( preg_match_all( $this->search_all['butts'], $this->Str, $matches_all ) ) {
      $type = 2;
    } else {
      $this->error( 'Таблица со стыками не распознана!', WARNING );
      return 0;
    }; // if
    if ( $debug )
      print_rt( $matches_all );

    $this->num_butts = count( $matches_all[0] );
    if ( $num_butts != $this->num_butts ) {
      $this->error( "Не все стыки распознаны ($num_butts != $this->num_butts)!", WARNING );
      return 0;
    }; // if
      
    $status_title = 'Кол-во стыков: ' . $this->num_butts;
    $butts          = array();		// массив стыков
    $repeats        = array();		// будем проверять на повторение стыков
    $this->kp       = True;		// уникальный километр
    $this->diameter = True;		// уникальный диаметр
    $this->kind     = 0;		// стыки должны быть идентичны

    for ( $i = 0; $i < $this->num_butts; $i++ ) {	// Перебираем все стыки в рапорте
      // Эта переменная нужна для отладки (подсказка для нумерации стыков в ВСН-таблице)
      $butts[$i]['origin'] = str_replace( '"', '', $matches_all[0][$i] );	// Используем при выводе
      $butts[$i]['origin'] = str_replace( "\t", "     \n", $butts[$i]['origin'] );

# $matches_all[ID]  - Обозначение сварного шва
      $wid = $butts[$i]['wid'] = $this->simplify( trim( $matches_all[ID][$i] ) );
      $repeat = $this->trim( $wid );
      if ( $butt = $this->decompound_butt_id( $wid ) ) {
        if ( isset( $repeats[$repeat] ) ) {
          $repeats[$repeat]++;
          $this->error( 'Стык повторяется: ' . $wid );
          $butt['sign'] .= '#' . $repeats[$repeat];
        } else
          $repeats[$repeat] = '1';
        $butts[$i] = array_merge( $butts[$i], $butt );

        // Проверка километра
        $kp = $butt['kp'];
        if ( $kp !== False ) {		// Если присутствует км в стыке
          if ( $this->kp === True )		//  Если это первый стык
            $this->kp = $kp;			//  то назначаем км
          if ( !is_bool( $this->kp ) AND ( $kp < $kp_min OR $kp > $kp_max ) ) {
            $this->error( "Проверьте километр: $wid ($kp: ожидается $kp_min<=x<=$kp_max)" );
            $this->kp = False;			// Провал проверки
          } elseif ( !is_bool( $this->kp ) AND ( $kp != $this->kp ) ) {
            $this->error( "Проверьте километр: $wid ($kp: ожидается $this->kp)", WARNING );
            $this->kp = False;			// Провал проверки
          }; // if
        }; // if

        // Выставляем тип
        $this->kind |= $butt['kind'];
      } else {
        $butts[$i]['pre']  = $wid;
        $butts[$i]['kind'] = False;
      }; // if
# $matches_all[WELDERS]  - Обозначение сварщика, не проверяем
      $welders = $this->simplify( $matches_all[WELDERS][$i] );
      if ( preg_match_all( '/((?:AG)?(?:W *)?(?: *- *)?[^ ,;]+)/', $welders, $matches ) ) {
        $wcount = count( $matches[0] );
        if ( $wcount == 2 ) {
          list( $key, $val ) = each( $matches[0] );
          $butts[$i]['welders'] = $val;
          list( $key, $val ) = each( $matches[0] );
          $butts[$i]['welders'] .= ', ' . $val;
        } else {
          $num = 0;
          $butts[$i]['welders'] = '';
          while( list( $key, $val ) = each( $matches[0] ) ) {
            if ( $num ) {
              $butts[$i]['welders'] .= ',';
              //if ( !( $num % ( $wcount > 9 ? 4 : 3 ) ) ) $butts[$i]['welders'] .= ' ';
              if ( !( $num % 4 ) ) $butts[$i]['welders'] .= ' ';
            }; // if
            $butts[$i]['welders'] .= $val;
            $num++;
          }; // while
        }; // if
      } else
        $this->error( 'Сварные не распознались: ' . $matches_all[WELDERS][$i], WARNING );
# $matches_all[THICKNESS]  - Толщина и диаметр, не проверяем
      $thickness = $this->simplify( $matches_all[THICKNESS][$i] );
      list( $butts[$i]['diameter'], $butts[$i]['thickness'] ) = $this->split_diameter( $thickness );

      // Проверка диаметра
      $diam = $butts[$i]['diameter'];
      if ( $this->diameter === True )			//  Если это первый стык
        $this->diameter = $diam;			//  то назначаем диаметр
      if ( !is_bool( $this->diameter ) AND ( $this->diameter != $diam ) ) {
        $this->error( "Проверьте диаметр: $wid ($diam: ожидается $this->diameter )", WARNING );
        $this->diameter = False;			// Провал проверки
      }; // if
# $matches_all[SOURCE]  - Расположение источника, не проверяем
      $butts[$i]['source'] = $matches_all[SOURCE][$i];
# $matches_all[DENSITY]  - Плотность, не проверяем
      $butts[$i]['density'] = $matches_all[DENSITY][$i];
# $matches_all[VWIRE]  - Видимый провод индикатора кач-ва, не проверяем
      $butts[$i]['v_wire'] = str_replace( ',', '.', $matches_all[VWIRE][$i] );
# $matches_all[DEFECTS]  - Дефекты, проверка при преобразовании дефектов ( ndtApiRtDefectsClass )
      $butts[$i]['defects'] = $this->simplify( $matches_all[DEFECTS][$i] );
# $matches_all[DECISION] - Решение
// бывает что в отчёте встречается два решения
// чтобы не писать слишком большой скрипт с проверкой, я просто перечисляю
// все возможные ситуации (здесь случаи когда оба решения пишутся через запятую)
      $butts[$i]['status'] = $this->trim( $matches_all[DECISION][$i] );

      switch( $butts[$i]['status'] ) {
// основные решения
        case 'A':	$butts[$i]['rt_status'] =  1; break;	// Принят
        case 'R':
        case 'R2':
        case 'RUT':	$butts[$i]['rt_status'] =  2; break;	// Ремонт
        case 'C':	$butts[$i]['rt_status'] =  8; break;	// Вырезать
// дополнительные
        case 'RX':						// Пересвет
        case 'RXUT':	$butts[$i]['rt_status'] = 16; break;
        case 'RXUTMT':	$butts[$i]['rt_status'] = 16;
			$butts[$i]['status'] = 'RxUM'; break;
        case 'UT':
        case 'UTMT':	$butts[$i]['rt_status'] = 32; break;	// UT DC
        case 'VT':	$butts[$i]['rt_status'] = 64; break;	// Не принят по ВИК
// смешанный rt_status
        case 'RRX':	$butts[$i]['rt_status'] = 18; break;	// и Ремонт и Пересвет
// Если Решений нет, то ошибка
        default:
          $butts[$i]['rt_status'] = 0;
          $this->error( "Решение не распознано: $wid" );
        }; // switch
# $matches_all[REMARKS] - Примечание
      $butts[$i]['remarks'] = $this->simplify( $matches_all[REMARKS][$i] );
    }; // for
    $this->butts = $butts;

    // Дополнительные проверки
    // Проверка на дату и последовательность рапортов
    // Проверка на оборудование
    // Проверка на фамилии
    // Проверка на ML RDX RLX RVX
    // Проверка на номер стыка 1-88
    // Проверка на толщину и диаметр
    return 1;
  } // function


  // Вызывается из run() и ndtApiRtClass::run()
  // Выводит стандартную таблицу ВСН
  // Для вывода дефектов использует ndtApiRtDefectsClass::vsn_table()
  function print_vsn_table ( ) {	// Выводит таблицу ВСН
    if ( !@include_once 'ndtApiRtDefectsClass.php' )
      user_error( 'Объект ( ndtApiRtDefectsClass ) не найден!', ERROR );
    $nastr = '&nbsp;';
    // Сохраняем инит-строку
    $saved_str = $this->Str;
    echo '<table border=1 class=vsn>';
    $j = 0;
    for ( $i = 0; $i < $this->num_butts; $i++ ) {	// Перебираем все стыки
      $butt = $this->butts[$i];
      // Если решение не принято, то пропускаем - пока не используем
      //if ( !( $butt['rt_status'] > 0 OR $butt['rt_status'] == -1 ) )
      //  continue;
      $j++;
// Номер
      echo "\n <tr>\n  <td class=vsn width=28>";
      echo "<span style=\"color:darkblue\" title=\"{$butt['origin']}\">$j</span>";
// Обозначение стыка
      // след строка ставить между S2B G123 неразрывнве пробелы
// !!!!!!!!
      $var = preg_replace( '/([^ ]+ [^ ]+ )(.*)/', '&nbsp;&nbsp;$1&nbsp;&nbsp; $2', $butt['wid'] );
      $var = $butt['wid'];
      echo "\n  <td class=vsn width=80><span style=\"color:darkblue\" title=\"{$butt['defects']}\">$var</span>";
// Толщина и диаметр
      echo "\n  <td class=vsn width=95>";
      echo sprint_dt( $butt, 1 );
// Обозначение сварщика
      echo "\n  <td class=vsn width=95>{$butt['welders']}";
// Дефекты
      echo "\n  <td class=vsn width=360>";
      // Для вывода таблицы дефектов для ВСН нам нужны две переменные
      $defects = $butt['defects'];	// Строка дефектов
      $v_wire = $butt['v_wire'];	// Толщина видимой проволки
      // Преобразование строк дефектов занимается свой объект
      $this->Str = $butt['defects'];	// У этого объекта своя инит-строка
      // передаём толщину проволки и диаметр
      $remarks2 = '';
      if ( $matches = ndtApiRtDefectsClass :: vsn_table( $butt['v_wire'], $butt['diameter'] ) ) {
        $str = '';
        for ( $k = 0; $k < count( $matches[0] ); $k++ ) {
          $str .= 'Pos: ' . $matches[1][$k];
          if ( $matches[2][$k] !== '' )
            $str .= '-' . $matches[2][$k];
          if ( $matches[3][$k] )
            $str .= '-' . $matches[3][$k];
          $str .= ' ';
        }; // for
        $remarks2 = $str;
      };
// Решение
      switch( $butt['rt_status'] ) {
        case  1:	echo "\n  <td class=vsn width=90>годен / accept"; break;
        case  2:	echo "\n  <td class=vsn width=90><b>исправить / to be repaired</b>"; break;
        case  8:	echo "\n  <td class=vsn width=90><span style=\"color: red\"><b>вырезать / to be cut out</b></span>"; break;
// Если решение не принято!
        case 16:	echo "\n  <td class=vsn width=90>";
                        $butt['remarks'] .= " Пересветить / Reshoot"; break;
        case 18:	echo "\n  <td class=vsn width=90><b>исправить / to be repaired</b>";
                        $butt['remarks'] .= " Пересветить / Reshoot"; break;
        case 32:	echo "\n  <td class=vsn width=90><span style=\"color: Sienna\"><i>UT</i></span>"; break;
        case 64:	echo "\n  <td class=vsn width=90><span style=\"color: Sienna\"><i>VT</i></span>"; break;
        default:	echo "\n  <td class=vsn width=90><span style=\"color: red\"><i>???</i></span>";
      }; // switch
// Примечание
      $butt['remarks'] = str_replace( 'Pos', 'Поз', $butt['remarks'] );
      $butt['remarks'] = str_replace( 'POS', 'Поз', $butt['remarks'] );
      $remarks2 = str_replace( 'Pos', 'Поз', $remarks2 );
      $remarks2 = str_replace( 'POS', 'Поз', $remarks2 );
      if ( $butt['remarks'] OR $remarks2 ) {
        echo "\n  <td class=vsn width=80>";
        if ( $butt['remarks'] )
          echo "<span style=\"color: Brown\">{$butt['remarks']}</span> ";
        if ( $remarks2 )
          echo $remarks2;
      } else
        echo "\n  <td class=vsn width=80>$nastr";

    }; // for
    echo '</table>';
    $this->Str = $saved_str;
    return 1;
  } // function
} // Класс ndtClass
?>
