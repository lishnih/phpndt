<?php	// Stan 27 июня 2006г.

// На входе:    Отчёт по радиографии API
// Результат:   Запись отчёта в базу данных
//              Преобразование API формы в ВСН форму
// На выходе:   ВСН форма
// Трассировка: Рапознанные переменные API формы
//              Процесс записи в базу данных
// trace()      Выводит $this

if ( !include_once '_ndtApiClass.php' )
  user_error( 'Объект ndtApiClass не найден!', ERROR );
if ( !include_once 'ndtApiRtButtsClass.php' )
  user_error( 'Объект ndtApiRtButtsClass не найден!', ERROR );


class ndtApiRtClass extends ndtApiClass {
  function ndtApiRtClass ( $str ) {
    $this->ndtApiClass( $str );
    $this->Name = basename( __FILE__, '.php' );

    // данные для поиска Км, Оборудования и стыков в отчётах
    // ( номер отчёта и дата описаны в ndtApiClass )
    // Обратите внимание - в поле kp ожидается два значения
    // они присвоятся переменным kp и kp2
    $this->search ['kp']        = '/(?:KP|запорной арматуры)[^\t]{1,20}\t{1,5}([^\t]+)\t([^\t]*)\t/';
    $this->search ['equipment'] = '/Equipment[^\t]{1,20}(?:[\t]+|  )([^\t]+)\t/';
    $this->search ['list']      = '/Длина дефектного индикаторного следа[^\n]{1,25}\n(.+)\nSUB-CONTRACTOR/s';
    $this->replace['kp']        = '<span style="color: darkred"><b>$0</b></span>';
    $this->replace['equipment'] = '<span style="color: darkred"><b>$0</b></span>';
    $this->replace['list']      = '<span style="color: blue"><b>$0</b></span>';

    return 1;
  } // Конструктор ndtClass

  // Эта функция вызывается из convert.php
  function run ( $view_mode = SHOW_VSN ) {      // Вывод на экран тоже разрешён
    // Следующие две функции просто распознают переменные, на экран ничего не выводят
    $this->recognize_api_singles();	// Распознаём номер отчёта, км и т.д. - объявлена в ndtParentClass
    $this->recognize_api_names();	// Распознаём фамилии - объявлена в ndtParentClass

    // Для распознавания информации о стыках создаём свой класс
    $ndtbuttclass = new ndtApiRtButtsClass( $this->list );
    //print_ra( $ndtbuttclass );
    ob_start();
    $butt_code = $ndtbuttclass->run();
    $content = ob_get_contents();
    ob_end_clean();
    //echo $content;

    // Выводим информацию о распозанной информации и таблицу ВСН
    if ( $view_mode & SHOW_INFO ) {
      $this->show_info();
      if ( $butt_code )
        $ndtbuttclass->show_info();
    }; // if
    if ( $view_mode & SHOW_VSN )
      echo $content;
    return True;
  } // function


  // Вызывается из run()
  function show_info ( ) {	// Показ инфы об отчёте
    echo "<pre>
<b>Информация об отчёте:</b>
report:      '$this->pre' $this->report '$this->sign'
kp:          $this->kp
site:        $this->site
kind:        " . decbin( $this->kind ) . "b
date:        $this->date
equipment:   $this->equipment
name0:       $this->name0
name1:       $this->name1
name2:       $this->name2
</pre>";
  } // function

} // Класс ndtClass
?>
