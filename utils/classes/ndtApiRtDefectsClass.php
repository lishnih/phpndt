<?php	// Stan 22 сентября 2006г.
if ( !include_once '__ndtParentClass.php' )
  user_error( 'Объект ndtParentClass не найден!', ERROR );

// На входе:    Строка дефектов одного стыка из рапорта API
// Результат:   Распознавание данных о дефектах
// На выходе:   Таблица дефектов одного стыка
// Трассировка: ---
// trace() родительская

// Константы для массива дефектов ( convert_defects )
  define( 'position', 0 );
  define( 'name',     1 );
  define( 'length',   2 );

class ndtApiRtDefectsClass extends ndtParentClass {
  function ndtApiRtDefectsClass ( $str ) {
    ndtParentClass :: ndtParentClass( $str );
    $this->Name = basename( __FILE__, '.php' );
    return 1;
  } // Конструктор ndtClass


  // Эта функция вызывается из convert.php
  function run ( ) {
    user_log( "Выполняется объект: $this->Name" );
    $this->vsn_table();
    return 1;
  } // function


  // Вызывается из run() и ndtApiRtButtsClass::print_vsn_table()
  // Принимает один параметр:
  // 1 - Толщина видимой проволки (необязательна)
  // Использует свою переменную Str в качестве строки из API-отчёта (строка дефектов
  // плюс позиция, если есть)
  // Если указаны позиции, то возращает массив позиций
  // Разбивает на несколько строк дефектов и вызывает для каждой vsn_table
  // -1 по умолчанию в позициях - значит эти параметры не передавались
  function vsn_table ( $v_wire = '0.32', $diameter = 48 ) {
    // След условие проверяет на ремонтные дефекты (по скобочкам)
    if ( preg_match_all( '/(?:POS:?)? *\([^\d]*(\d{1,3})(?: *(?:- *(00?) *)?- *(\d{1,3}))?\)([^(]*)/i', $this->Str, $matches ) ) {
      //print_ra( $matches );
      for ( $k = 0; $k < count( $matches[0] ); $k++ ) {
        // Эта функция может быть вызвана из другого класса, поэтому в
        // cледующей строке функция вызывается явно из этого класса
        $str = preg_replace( '/ *Pos */i', '', $matches[4][$k] );
        $start = is_numeric( $matches[1][$k] ) ? (int) $matches[1][$k] : False;
        $stop  = is_numeric( $matches[3][$k] ) ? (int) $matches[3][$k] : False;
        //			строка дефектов, толщина проволки, диаметр, начало / конец диапазона
        if ( $stop === False )
          ndtApiRtDefectsClass :: convert_defects( $str, $v_wire, $diameter, $start, $start );
        elseif ( $start > $stop ) {
          $max_length = round( ( $diameter * 25.4 + 0.8 ) * pi() / 10 );
          $max_length = ceil( $max_length / 10 ) * 10;
          if ( $stop )
            ndtApiRtDefectsClass :: convert_defects( $str, $v_wire, $diameter, 0, $stop );
          ndtApiRtDefectsClass :: convert_defects( $str, $v_wire, $diameter, $start, $max_length );
        } else
          ndtApiRtDefectsClass :: convert_defects( $str, $v_wire, $diameter, $start, $stop );
      }; // for
      return $matches;
    } else {	// Если скобочек нет, значит перед нами полный стык
      ndtApiRtDefectsClass :: convert_defects( $this->Str, $v_wire, $diameter );
      return 0;
    }; // if
  } // function


  // Вызывается из vsn_table()
  // Принимает три параметра:
  // 1 - Строка дефектов API-стандарта для одного стыка
  // 2 - Толщина видимой проволки
  // 3 - Диапазон (необязателен)
  // Выводит в броузер таблицу дефектов по ВСН-стандарту для одного стыка
  // -1 в позициях - значит не заданы
  function convert_defects ( $str, $v_wire, $diameter, $start_r = -1, $max_r = -1 ) {
  global $start_pos, $stop_pos, $piece, $emp;
    $max_length = round( ( $diameter * 25.4 + 0.8 ) * pi() / 10 );
    $max_length = ceil( $max_length / 10 ) * 10;
    if ( $start_r > -1 ) {	// Если заданы позиции
      $start = $start_r;
      $max   = $max_r;
      if ( $max > $max_length )
        user_error( "Конечная позиция превышает допустимый максимум: $max", WARNING );
    } else {
      $start = 0;		// Начинаем с этой позиции
      $max   = $max_length;	// Максимальное значение позиции
    }; // if
    // $max_str = 48 или 53	// максимамльное кол-во строк для вывода в таблицу

    echo "\n   <table width=100% class=vsn>";
    $str = str_replace( ',', '.', $str );	// Чтобы длина правильно вычислялать
    $str = str_replace( ' ', '', $str );	// убираем оставшиеся пробелы
    $v_wire = str_replace( '.', ',', $v_wire );	// В ВСН должны быть запятые
    $Parts = explode( '/', $str );		// Массив кодов

    // Мы будем использовать srand, чтобы один и тот же отчёт не выдавал
    // новый набор при каждом новом выводе
    srand( crc32( $str ) );

    if ( !$Parts[0] ) {
      user_error( 'Не задана строка дефектов', WARNING );
      echo "\n   </table>";
      return 0;
    }; // if
    if ( preg_match( '/- *- *-/', $Parts[0] ) ) {
      if ( $start <= '12' )
        $start = '&nbsp;' . $start;
      $pos_str = $max != $start ? "$start-$max" : $start;
      echo <<<EOD
\n    <tr><td class=def width=30%>$pos_str<td class=def width=25%>$v_wire<td class=def>$emp
   </table>
EOD;
      return 1;
    }; // if
    $defect = '';			// На случай, если первый код - позиция
    $numberset = 1;			// Для проверки на два дефекта подряд
    $pos = 0;				// Проверяем чтобы позиция шла на увеличение
    while( list( $key, $val ) = each( $Parts ) ) {
      $str = trim( $val );		// Читаем текущий код (удаляем пробелы и табуляцию)
      if ( $str == '0' OR $str ) {	// Нулевая позиция даёт False, поэтому обозначаем
	// Если число (позиция)
        if ( is_numeric( $str ) ) {
          $str = (float) $str;
          if ( $str < $start )
            user_error( "Дефект вне заданного интервада: $str < $start", WARNING );
          if ( $pos - $str >= 1 )
            user_error( "Проверьте последовательность позиций: $pos / $str", WARNING );
          if ( $str > $max ) {
            user_error( "Позиция перешла за максимум: $str", WARNING );
            $str = $pos;
          } else
            $pos = $str;
          $len = 0;
          if ( $defect ) {
            if ( $defect != 'TI' )		// !!!!!!!!
              $m01[] = array( position => $str, name => $defect, length => $len );
          } else
            user_error( "Не задан дефект: $str", WARNING );
          $numberset = 1;
	// Если протяжённость (позиция1-позиция2)
        } elseif ( preg_match( '/^(\d{1,5}.?\d{0,2})-(\d{1,5}.?\d{0,2})$/', $str, $matches ) ) {
          $str = (float) $matches[1];
          if ( $str < $start )
            user_error( "Дефект вне заданного интервада: $str < $start", WARNING );
          if ( $pos - $str >= 1 )
            user_error( "Проверьте последовательность позиций: $pos / $str", WARNING );
          if ( $str > $max ) {
            user_error( "Позиция перешла за максимум: $str", WARNING );
            $str = $pos;
          } else
            $pos = $str;
          //$len = ( $matches[2] ) ? $matches[2] : $max;	// Пример 360-0
          $len = (float) $matches[2];
          // После такого примера уже не должны идти дефекты, пока не проверяется
          if ( $len > $max )
            user_error( "2ая позиция перешла за максимум: $len", WARNING );
          $len = ( $len - $str ) * 10;
          if ( $len < 0 )
            user_error( "Протяжённость отрицательна: $str($len)", WARNING );
          if ( $defect )
            $m01[] = array( position => $str, name => $defect, length => $len );
          else
            user_error( "Не задан дефект: $str", WARNING );
          $numberset = 1;
	// Если строка (код дефекта)
        } elseif ( preg_match( '/^[A-Za-z]+$/', $str ) ) {
          if ( $numberset ) {
            $defect = $str;			// устанавливаем дефект
            $numberset = 0;
          } else
            user_error( "Задано 2 дефекта: $defect / $str", WARNING );
        } else {				// Иначе - неопознанная комбинация
          user_error( "Неопознанные символы: $str", WARNING );
          $defect = $str;
          $numberset = 0;
        }; // if
      }; // if
    }; // while
    //print_ra( $m01 ); return;

    // Если не встретились дефекты (кроме TI)
    if ( !isset( $m01 ) ) {
      if ( $start <= '12' )
        $start = '&nbsp;' . $start;
      $pos_str = $max != $start ? "$start-$max" : $start;
      echo <<<EOD
\n    <tr><td class=def width=30%>$pos_str<td class=def width=25%>$v_wire<td class=def>$emp
   </table>
EOD;
      return 1;
    }; // if

    $count_p = 0;
    // Теперь преобразуем английскую кодировку на русскую и ставим протяжённость
    $next = 0;
    $stop_p = 0;
    while( list( $key, $m ) = each( $m01 ) ) {
      if ( $count_p )			// Рассыпаем поры, если есть, между дефектами
        while( $next < $m[position] ) {
          $m02[] = array( $next, 'Aa_def_a' );
          $next += 10;
          $count_p--;
          if ( !$count_p )		// Если пор больше нет то прекращаем
            break;
          elseif ( $count_p == 1 )	// Если последняя пора, то присваиваем ей последнюю позицию
            $next = $stop_p;
        }; // while
      switch ( strtoupper( $m[name] ) ) {
// поры - с ними особый случай когда пишут, к примеру, P/100-120/
        case 'P':	$name = 'Aa_def_a';
          if ( $m[length] ) {
            $start_p = $m[position];
            $stop_p  = $m[position] + $m[length] / 10;	// длина считается в миллиметрах
            $count_p = ceil( $m[length] / 100 ) - 1;	// Одну пору уже учли
            $next = $start_p + 10;			// следущую пору поставим через 10 мм
          };
          break;
        // Есть кодировка для AGI
        case 'RI':	$name = 'Aa_def_a'; break;
// скопление пор
        case 'CI':                          // AGI
        case 'CP':
        case 'AI':	$name = 'Ac' . ( $m[length] ? $m[length] : '_defe' ) . '-_def_a' ; break;
        case 'HB':	$name = 'Ak' . ( $m[length] ? $m[length] : '_defe' ); break;
// шлаки
        case 'SI':                          // AGI
        case 'ISI':	$name = 'Ba' . ( $m[length] ? $m[length] : '_def_b' ); break;
        case 'EI':                          // AGI
        case 'ESI':	$name = 'Bd' . ( $m[length] ? $m[length] : '_defe' ); break;	//x0,8
// непровар / несплавление
        case 'IP':
        case 'IPD':	$name = 'Da' . ( $m[length] ? $m[length] : '_defe' ); break;
        case 'IF':
        case 'IFD':	$name = 'Dc' . ( $m[length] ? $m[length] : '_defe' ); break;
// утяжина
        case 'BT':
        case 'IC':
        case 'ICP':	$name = 'Fa' . ( $m[length] ? $m[length] : '_defe' ); break;
// подрез
        case 'UС':                          // AGI
        case 'EU':
        case 'IU':	$name = 'Fc' . ( $m[length] ? $m[length] : '_defe' ); break;
// остальное
        case 'C':	$name = 'Ea' . ( $m[length] ? $m[length] : '_defe' ); break;
        case 'FB':	$name = 'Fb' . ( $m[length] ? $m[length] : '_defe' ); break;
// вольфрам не указываем
        case 'TI':	$name = ''; break;       // AGI
        default:
          user_error( "--{$m[name]}--" );
          $name = '<span style="color: red"><b>' . $m[name] . '</span></b>';
      }; // switch
      if ( $name )
        $m02[] = array( $m[position], $name );
    }; //while
    // Если остались поры, то рассыпаем их
//echo "$start_p,$stop_p,$count_p,$next,{$m[position]}";
    if ( $count_p )			// Рассыпаем поры, если есть, между дефектами
      while( $next < $max ) {
        $m02[] = array( $next, 'Aa_def_a' );
        $next += 10;
        $count_p--;
        if ( !$count_p )		// Если пор больше нет то прекращаем
          break;
        elseif ( $count_p == 1 )	// Если последняя пора, то присваиваем ей последнюю позицию
          $next = $stop_p;
      }; // while
    //print_ra( $m02 ); return;

    // Разбиваем весь диапазон на заданные отрезки, подсчитываем кол-во дефектов
    $position = $start;
    while( list( $key, $m ) = each( $m02 ) ) {
      if ( $m[position] - $position >= $piece ) {	// Пустой диапазон + следующий с дефектами
        // Считываем первое значение позиции, если до неё от текущей позиции больше 300 мм,
        // значит до этой позиции "дно", отмечаем его:
        $fl = floor( $m[position] / 10 ) * 10;
        $fl = floor( $fl / $piece ) * $piece;
        $m03[ $fl ] = $emp;
        $position = ( $fl + $piece ) > $max ? $max : $fl + $piece;
        // Затем в новом диапазоне выводим сам дефект, а также заносим его в таблицу дефектов,
        // чтобы не было повторов одноимённых дефектов
        $m03[ $position ][ $m[name] ] = isset( $m03[ $position ][ $m[name] ] ) ? $m03[ $position ][ $m[name] ] + 1 : 1;
      // Если диапазон с дефектами продолжается
      // Если поставить <= 0 то границы будут относиться к меньшему диапазону
      } elseif ( $m[position] - $position < 0 & $position !=0 ) {
        $m03[ $position ][ $m[name] ] = isset( $m03[ $position ][ $m[name] ] ) ? $m03[ $position ][ $m[name] ] + 1 : 1;
      // // Если закончился, то следующий диапазон
      } else {
        $position = ( $position + $piece ) > $max ? $max : $position + $piece;
        $m03[ $position ][ $m[name] ] = isset( $m03[ $position ][ $m[name] ] ) ? $m03[ $position ][ $m[name] ] + 1 : 1;
      }; // if
    }; // while
    //print_ra( $m03 ); return;
    unset( $m02 );

    // Выводим на экран
    // будем генерить случаянные значения для пор ( массив от 2 до 7 потому что генератор
    // числе используем для пор и шлаков )

    $aa_array = array( '', '', '0,5', '0,5', '0,8', '1', '1', '1,5' );
    $position = $start;
    // Ворд тупит и заменяет 10 на месяц, обламываем его
    if ( $position <= '12' )
      $position = '&nbsp;' . $position;
    while( list( $key, $m ) = each( $m03 ) ) {	// каждая ячейка - это диапазон 300мм
      echo "\n    <tr><td class=def width=30%>" . $position;
      // Бывает, что отрезок задан только одной позицией, делаем проверку
      if ( $position != $key )
        echo '-' . $key;
      $position = $key;
      echo "<td class=def width=25%>$v_wire<td class=def>";
      $sumlen = 0;		// вычисляем общую протяжённость всех дефектов
      if ( is_array( $m ) ) {
        $num = 0;
        while( list( $key, $val ) = each( $m ) ) {
          if ( $num ) {
            echo ';';
            if ( !( $num % 3 ) ) echo ' ';
          }; // if
          $num++;
          // Преобразование при выводе дефектов длиной 5мм (обозначен _defe'ом)
          $k = rand( 4, 9 );
          //$key = preg_replace( '/^([a-z]{2})5$/i', "$1$k'", $key, $match ); // почему-то не получается
          //if ( preg_match( '/^([a-z]{2})5$/i', $key, $match ) )	// старый вариант
          //  $key = $match[1] . $k;
// расчёт некоторых протяжённостей дефектов
          switch ( $key ) {
            case 'Dc_defe':
              $k1 = floor( 28 / $val );
              if ( $k1 > 1 AND $k1 < $k )
                $k = $k1;
              break;
            case 'Da_defe':
              $k1 = floor( 24 / $val );
              if ( $k1 > 1 AND $k1 < $k )
                $k = $k1;
              break;
            case 'Ac_defe-def_a':
              $k1 = floor( 30 / $val );
              if ( $k1 > 1 AND $k1 < $k )
                $k = $k1;
              break;
          }; // switch
          $key = str_replace( '_defe', $k, $key );	// новый вариант (заменяется _defe)
          // Преобразование при выводе пор и шлаков
          $k = rand( 2, 7 );
          $key = str_replace( '_def_a', $aa_array[$k], $key );	// Aa и Ac
          $key = str_replace( '_def_b', $k, $key );		// Bd
// проверка на протяжённость дефектов и выставление знака < или >
          $deflen = 0;		// флаг наличия ремонтов
          if ( preg_match( '/([A-z]+)([^A-z]*)$/i', $key, $match ) ) {
            switch( $match[1] ) {
              case 'Ak':	if ( $match[2]        > 13 ) $deflen = 1;
				if ( $match[2] * $val > 50 ) $deflen = 1;	break;
              case 'Da':	if ( $match[2] * $val > 25 ) $deflen = 1;	break;
              case 'Ac':
              case 'Dc':	if ( $match[2] * $val > 30 ) $deflen = 1;	break;
              case 'Bd':
              case 'Fb':
              case 'Fc':	if ( $match[2] * $val > 50 ) $deflen = 1;	break;
              case 'Ea':	$deflen = 1;					break;
              default:		$deflen = 0;
            }; //switch
            $sumlen += $match[2] * $val;	// считаем общую протяжённость
          }; // if
// Выводим дефект
          if ( $deflen )
            echo "<b><span style=\"color: red\">$key-$val></span></b>";
          elseif ( preg_match( '/[A-z]{2}[0-9]{2,}/i', $key ) )
            echo "<span style=\"color: Brown\">$key-$val<</span>";
          else
            echo "$key-$val<";
          //if ( $val != 1 ) echo "-$val<";	// Кол-во дефектов
        }; // while
      } else		// скорее всего это дно
        echo $m;
      //if ( $sumlen > 308 )
      //  echo '<span style="color: red;background-color: black">!!!</span>';
    }; // while

    if ( $position < $max ) echo "\n    <tr><td class=def>".$position.'-'.$max."<td class=def>$v_wire<td class=def>".$emp;
    //echo "\n    <tr><td width=30%><td width=25%><td width=45%>";
    unset( $m03 );
    echo "\n   </table>";
    return 1;
  } // function
} // Класс ndtClass
?>
