<?php // Stan February 17, 2009

  $title = "Просмотр по дате";
  include '../_local.php';

  $url = "index.php";
  $reports_url = "reports.php";

  $method = 'ВИК';

  $hour_corrective = 4;
  $timestamp_corrective = $hour_corrective * 60 * 60;

  function tr_day( $d, $m, $y ) {
  global $url, $reports_url, $timestamp_corrective, $get_str, $meta_get;
    $date      = mktime( 0, 0, 0, $m, $d, $y ) + $timestamp_corrective;
    $date_str  = date( 'd.m.Y', $date );
    $url1      = sum_gets( $get_str, 'reports[date][<]=' . $date );
    $url2      = sum_gets( $get_str, 'reports[date]='    . $date );
    $url3      = sum_gets( $get_str, 'reports[date][>]=' . $date );

    $count1 = $meta_get->sql_select_count( 'reports', 'reports.date<' . $date );
    $count2 = $meta_get->sql_select_count( 'reports', 'reports.date=' . $date );
    $count3 = $meta_get->sql_select_count( 'reports', 'reports.date>' . $date );

    $str    = <<<EOD
<tr>
<td><a href="$url?$url1">$date_str</a> | (<a href="$reports_url?$url1">$count1</a>)
<td><a href="$url?$url2">$date_str</a> | (<a href="$reports_url?$url2">$count2</a>)
<td><a href="$url?$url3">$date_str</a> | (<a href="$reports_url?$url3">$count3</a>)
\n
EOD;
    return $str;
  } // function

  function tr_month( $m, $y ) {
  global $url, $reports_url, $timestamp_corrective, $get_str, $meta_get;
    $date      = mktime( 0, 0, 0, $m,     1, $y ) + $timestamp_corrective;
    $date_inc  = mktime( 0, 0, 0, $m,     1, $y ) + $timestamp_corrective - 1;
    $date1     = mktime( 0, 0, 0, $m + 1, 1, $y ) + $timestamp_corrective;
    $date1_inc = mktime( 0, 0, 0, $m + 1, 1, $y ) + $timestamp_corrective - 1;
    $date_str  = date( 'm.Y', $date );
    $url1      = sum_gets( $get_str, 'reports[date][<]=' . $date );
    $url2      = sum_gets( $get_str, 'reports[date][>]=' . $date_inc . '&reports[date][<]=' . $date1 );
    $url3      = sum_gets( $get_str, 'reports[date][>]=' . $date1_inc );

    $count1 = $meta_get->sql_select_count( 'reports', 'reports.date<' . $date );
    $count2 = $meta_get->sql_select_count( 'reports', array( 'reports.date>' . $date_inc, 'reports.date<' . $date1 ) );
    $count3 = $meta_get->sql_select_count( 'reports', 'reports.date>' . $date1_inc );

    $str    = <<<EOD
<tr>
<td><a href="$url?$url1">$date_str</a> | (<a href="$reports_url?$url1">$count1</a>)
<td><a href="$url?$url2">$date_str</a> | (<a href="$reports_url?$url2">$count2</a>)
<td><a href="$url?$url3">$date_str</a> | (<a href="$reports_url?$url3">$count3</a>)
\n
EOD;
    return $str;
  } // function

  $month = date('m');
  $day   = date('d');
  $year  = date('Y');
  $today = mktime( 0, 0, 0, $month, $day, $year );

  echo 'Сейчас: <b>' . strtotime( 'now' ) . ' (' .
       date( 'd.m.Y H:i:s O') . ') [' . $hour_corrective . "]</b>\n";

  echo "<table border=1 style='text-align:center'>";
  echo "<tr><th>До указанной даты<th>В этот день<th>После указанной даты";
  echo tr_day( $day,      $month, $year );
  echo tr_day( $day -  1, $month, $year );
  echo tr_day( $day -  2, $month, $year );
  echo tr_day( $day -  3, $month, $year );
  echo tr_day( $day -  4, $month, $year );
  echo tr_day( $day -  5, $month, $year );
  echo tr_day( $day -  6, $month, $year );
  echo tr_day( $day -  7, $month, $year );
  echo tr_day( $day -  8, $month, $year );
  echo tr_day( $day -  9, $month, $year );
  echo tr_day( $day - 10, $month, $year );
  echo tr_day( $day - 11, $month, $year );
  echo tr_day( $day - 12, $month, $year );
  echo tr_day( $day - 13, $month, $year );
  echo tr_day( $day - 14, $month, $year );
  echo "</table>";

  echo "<br />\n";

  echo "<table border=1 style='text-align:center'>";
  echo "<tr><th>До указанного месяца<th>В этот месяц<th>После указанного месяца";
  echo tr_month( $month,      $year );
  echo tr_month( $month - 1,  $year );
  echo tr_month( $month - 2,  $year );
  echo tr_month( $month - 3,  $year );
  echo tr_month( $month - 4,  $year );
  echo tr_month( $month - 5,  $year );
  echo tr_month( $month - 6,  $year );
  echo tr_month( $month - 7,  $year );
  echo tr_month( $month - 8,  $year );
  echo tr_month( $month - 9,  $year );
  echo tr_month( $month - 10, $year );
  echo tr_month( $month - 11, $year );
  echo tr_month( $month - 12, $year );
  echo "</table>\n<br />\n";

  $m_1 = $month - 1;
  $date = mktime( 0, 0, 0, $m_1, 24, $year ) + $timestamp_corrective;
  $m_1_str = sprintf( "%02d", $m_1 );
  echo "Заключения с <a href=\"$url?mode=reports&reports[method]=$method&reports[date][>]=$date\">25.$m_1_str.$year</a> [$method]";
?>
