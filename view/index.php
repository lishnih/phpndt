<?php // Stan 2007-11-18

  $title = "Просмотр";
  include '../_local.php';
//check_user( $user, 1 );       // Авторизуем пользователя

  echo '<table cellpadding="0" cellspacing="0" border="0" width="100%">';
  echo '<tr><td width="52%">';
  echo '<a href="?' . sum_gets( 'mode=reports',  $get_str ) . '">Заключения</a> :: ';
  echo '<a href="?' . sum_gets( 'mode=joints',   $get_str ) . '">Стыки</a> :: ';
  echo '<a href=".">сброс</a>' . "<br />\n";

  echo '<td style="text-align:center;vertical-align:top">';
  $kp = $meta_get->get('joints', 'kp');
  if ( $kp and !is_array( $kp ) )
    echo '<a href="?' . sum_gets( "mode=$mode", $get_str ) . '&joints[kp]=' . ($kp - 1) . '">&lt;</a> ' . $kp .
        ' <a href="?' . sum_gets( "mode=$mode", $get_str ) . '&joints[kp]=' . ($kp + 1) . '">&gt;</a>';

  echo '<td width="32%" style="text-align:right;vertical-align:top">' .
       '<a href="date.php">Выбор по дате</a> :: ' .
       '<a href="..">Меню</a>' . "<br />\n";

  echo "</table>\n<hr />\n";

  if ( $show_table ) {
    echo $show_table . "<br />\n";
    $rows = $meta_get->sql_select( '*', $show_table );
    print_rt($rows);
  }
  elseif ( file_exists( "$mode.php" ) )
    include "$mode.php";
  else {
    // Выводим основной header (параметры стыков)
    echo '<a href="?' . sum_gets( 'options=linked', $get_str ) . '">связать таблицы (медленнее)</a> ';
    echo array_search( 'linked', $options ) !== False ? '[включено]' : '[отключено]';
    echo ' :: <a href="?' . sum_gets( 'options=debug', $get_str ) . '">отладка</a>' . "<br />\n";

    switch ( $mode ) {
    case 'reports':
      $rows = $meta_get->sql_select( '*', 'reports', 'report' );
      break;
    case 'joints':
      $rows = $meta_get->sql_select( '*', 'reports,mjoints,joints' );
      break;
    case '':
      break;
    default:
      echo '<span style="color: red">Неизвестный режим: ' . $mode . "!</span><br />\n";
    }; // switch
    if ( isset( $rows ) ) {
      print_rt( $rows );
      echo "<hr />\n";
    } // if

    view_header_all();
  } // if

  if ( array_search( 'debug', $options ) !== False ) {
    print_debug();
  } // if
?>
