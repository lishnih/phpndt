<?php // Stan 18 ноября 2007г.

  $title = "Стыки";
  include '../_local.php';
//check_user( $user, 1, 'rt' );         // Авторизуем пользователя


  $jc = 0;  // Контроль непрерывности стыков

  echo '<a href="?' . sum_gets( 'mode=joints', $get_str ) . '">&lt;&lt;</a>' . "<br />\n";


  $rows = $meta_get->sql_select( 'DISTINCT *',
                                 'joints',
                                 'joints.joint_pre, joints.joint_line, joints.joint_seq' );
//print_rt( $rows );

  if ( $rows ) {
    start_table( array( '#', 'id' => 1, '!dont_close_thead' => 1 ),  # Не закрываем THEAD
                 'Стык',
                 array( 'Диаметр', 'td' => 'align=center' ),
                 array( 'Толщина', 'td' => 'align=center' )
               );

    $methods = $meta_get->sql_select( 'DISTINCT method', 'reports', 'method' );
    foreach( $methods as $method ) {
      $method = $method['method'];
      echo "   <th>$method</th>\n";
    } // foreach
    echo "  </tr>\n </thead>\n <tbody>\n";

    $joint_seq = $multiple;
    foreach( $rows as $key => $row ) {

      if ( $jc ) {
        $diff = (int) ( ( $row['joint_seq'] - $joint_seq ) / $multiple );
        $str = '';
        if     ( $diff >  1 )
          $str = sprint_color( $joint_seq . ' - ' . ($row['joint_seq'] - 1), 'Yellow', 'Black' );
        elseif ( $diff == 1 )
          $str = sprint_color( $joint_seq, 'Yellow', 'Black' );
        elseif ( $diff < -1 )
          $str = sprint_color( '= = =', 'Magenta', 'Black' );
  
        if ( $str )
          echo '<tr><td><td colspan=3><center><b>' . $str . "</b></center>\n";
      } // if

      $reports_array = array();
      foreach( $methods as $method ) {
        $method = $method['method'];
        $rows2 = $meta_get->sql_append_select( '*, reports.name as reports_name, reports.id as reports_id',
                                               'reports,register_entries',
                                               array('register_entries._joints_id=' . $row['id'], 'register_entries.method="' . $method . '"'),
                                               'reports.report_pre, reports.report_seq, reports.method, reports.date' );
        $reports_array[] = sprint_reports( $rows2 );
      } // foreach
      $reports_str = implode('</td><td>', $reports_array);

      echo_tr( 1,
               link_joint( $row ),
               sprint_diameter( $row ),
               sprint_thickness( $row ),
               $reports_str );

      $joint_seq = $row['joint_seq'] + $multiple;

    }; // foreach

    if ( $jc ) {
      $str = sprint_color( 'Заканчивается стыком: ' . get_name( $row ),
             'Magenta', 'Black' );
      echo '<tr><td><td colspan=4><center><b>' . $str . "</b></center>\n";
    } // if

    stop_table();
  }; // if

  if ( array_search( 'debug', $options ) !== False )
    print_debug();


  function sprint_reports( $rows, $method_req = False ) {
    $reports_str = '<table border=1>';
    foreach( $rows as $row ) {
      $td_method = $method_req ? '<td>' . $row['method'] : '';
      $reports_str .= '<tr>' .
              $td_method .
              '<td>' . link_report( $row, 'reports_name', 'reports_id' ) .
              '<td>' . sprint_date( $row ) .
              '<td>' . sprint_decision( $row['decision'] ) . "\n";
    } // foreach
    $reports_str .= '</table>';
    return $reports_str;
  } // function
?>
