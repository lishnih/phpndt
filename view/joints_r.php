<?php // Stan 10 марта 2011г.

  $title = "Стыки";
  include '../_local.php';
//check_user( $user, 1, 'rt' );         // Авторизуем пользователя

  // Отображает стыки которые были отданы в ремонт и не отремонтированы

  $rows = $meta_get->sql_select( 'distinctrow joints.*', 'reports,mjoints,joints', 'joints.kp,joints.number,joints.type,joints.sign' );
//print_rt( $rows );

  if ( $rows ) {
    start_table( array( '#',         'id' => 1 ),
                 'Стык',
                 array( 'Диаметр',   'td' => 'align=center' ),
                 array( 'Толщина',   'td' => 'align=center' ),
                 array( 'Категория', 'td' => 'align=center' ),
                 array( 'Сварщик',   'td' => 'align=center' ),
                 array( '<',         'td' => 'align=center' ),
                 array( 'ВИК',       'td' => 'align=center' ),
                 array( '>',         'td' => 'align=center' ),
                 array( '<',         'td' => 'align=center' ),
                 array( 'РК',        'td' => 'align=center' ),
                 array( '>',         'td' => 'align=center' ),
                 array( '<',         'td' => 'align=center' ),
                 array( 'УЗК',       'td' => 'align=center' ),
                 array( '>',         'td' => 'align=center' )
               );

    foreach( $rows as $key => $row ) {
      // Выбираем неремонтные стыки
      $rows_vt = $meta_get->sql_append_select( '*', 'reports,mjoints', "reports.method='VT' AND mjoints.signR='' AND mjoints.joint_id_='{$row['id']}'", 'mjoints.signE,date' );
      $rows_rt = $meta_get->sql_append_select( '*', 'reports,mjoints', "reports.method='RT' AND mjoints.signR='' AND mjoints.joint_id_='{$row['id']}'", 'mjoints.signE,date' );
      $rows_ut = $meta_get->sql_append_select( '*', 'reports,mjoints', "reports.method='UT' AND mjoints.signR='' AND mjoints.joint_id_='{$row['id']}'", 'mjoints.signE,date' );

      $rows_test = $meta_get->sql_append_select( '*', 'reports,mjoints', "reports.method='RT' AND reports.enabled!='0' AND mjoints.signR='' AND mjoints.joint_id_='{$row['id']}'", 'mjoints.signE,date' );
      if ( !isset($rows_test[0]) or $rows_test[0]['decision'] != 'ГОДЕН' ) {   // !!!
//      print_rt( $rows_test );
        $rows_r_vt = $meta_get->sql_plain_select( "SELECT * FROM reports,mjoints WHERE reports.id=mjoints.report_id_ AND reports.method='VT' AND reports.enabled='1' AND mjoints.signR!='' AND mjoints.joint_id_='{$row['id']}' ORDER BY `mjoints`.`signE`, `date`" );
        $rows_r_rt = $meta_get->sql_plain_select( "SELECT * FROM reports,mjoints WHERE reports.id=mjoints.report_id_ AND reports.method='RT' AND reports.enabled='1' AND mjoints.signR!='' AND mjoints.joint_id_='{$row['id']}' ORDER BY `mjoints`.`signE`, `date`" );
        $rows_r_ut = $meta_get->sql_plain_select( "SELECT * FROM reports,mjoints WHERE reports.id=mjoints.report_id_ AND reports.method='UT' AND reports.enabled='1' AND mjoints.signR!='' AND mjoints.joint_id_='{$row['id']}' ORDER BY `mjoints`.`signE`, `date`" );

        $rows_test = $meta_get->sql_append_select( '*', 'reports,mjoints', "reports.method='RT' AND reports.enabled!='0' AND mjoints.signR!='' AND mjoints.joint_id_='{$row['id']}'", 'mjoints.signE,date' );
        if ( !isset($rows_test[0]) or $rows_test[0]['decision'] != 'ГОДЕН' ) {   // !!!
//        print_rt( $rows_test );
          echo_tr( 1,
                 sprint_joint( $row, 0 ),
                 sprint_diameter( $row ),
                 "'" . sprint_thickness( $row ),
                 sprint_f($rows_rt, 'q_level'),
                 sprint_f($rows_rt, 'welders'),
                 sprint_td( $rows_vt ),
                 sprint_td( $rows_rt ),
                 sprint_td( $rows_ut ) );

          if ( $rows_r_vt or $rows_r_rt or $rows_r_ut )
            echo_tr( null ,
                     sprint_color( '< Ремонт >', 'Brown', 'Gold' ),
                     sprint_diameter( $row ),
                     "'" . sprint_thickness( $row ),
                     sprint_f($rows_rt, 'q_level'),
                     sprint_f($rows_rt, 'welders'),
                     sprint_td( $rows_r_vt ),
                     sprint_td( $rows_r_rt ),
                     sprint_td( $rows_r_ut ) );
        } // if
      } // if
    }; // foreach

    $str = sprint_color( 'Этот км заканчивается стыком: ' . sprint_joint( $row, 0 ), 'Magenta' );
    echo '<tr style="background-color:Gray"><td><td colspan=14><center><b>' . $str . "</b></center>\n";
    stop_table();
  }; // if

  if ( array_search( 'debug', $options ) !== False )
    print_debug();


  function sprint_td( $rows ) {
    $str_1 = $str_2 = $str_3 = '';
    foreach( $rows as $row ) {
      $decision = $row['decision'];
      $str_1 .= link_report( $row ) . '<br />';
      $str_2 .= sprint_date( $row ) . '<br />';
      $str_3 .= sprint_decision( $row['decision'] ) . '<br />';
    } // foreach
    return $str_1 . '<td align=center>' . $str_2 . '<td align=center>' . $str_3 . "\n";
  } // function

  function sprint_f( $rows, $field ) {
    $str = '';
    foreach( $rows as $row )
      $str .= $row[$field] . '<br />';
    return $str;
  } // function
?>
