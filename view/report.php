<?php // Stan 13 июля 2006г.

  $title = 'Заключение';
  include '../_local.php';
//check_user( $user, 1, 'rt' );         // Авторизуем пользователя

  echo '<a href=".?' . $get_str . '">&lt;&lt;</a>' . "<br />\n";

  $rows = $meta_get->sql_select( '*',
                                 'reports',
                                 'reports.report_pre, reports.report_seq, reports.method, reports.date', 0 );
  print_rt( $rows );

  if ( $rows ) {
    echo 'Скан: ' . link_scan_report( $rows[0] ) . "<br />\n";

    echo "Стыки:<br />\n";
    $rows = $meta_get->sql_select( '*, joints.name as joints_name, joints.id as joints_id',
                                   'reports,joints,register_entries',
                                   'joints.joint_pre, joints.joint_line, joints.joint_seq' );
    $i = 0;
    echo "<table><tr>\n";
    foreach( $rows as $row ) {
      echo '<td>' . link_joint( $row, 'joints_name', 'joints_id' ) . '</td>';
      $i++;
      if ( $i == 5 ) {
        echo "\n</tr><tr>\n";
        $i = 0;
      } // if
    } // foreach
    echo "</tr></table>\n<br />\n";

    echo "Отладка<br />\n";
    print_rt( $rows );

    $rows = $meta_get->sql_select( '*, dirs.name as dirs_name, files.name as files_name, sheets.name as sheets_name',
                                   'tasks,dirs,files,sheets,reports,register_entries',
                                   'reports.report_pre, reports.report_seq, reports.method, reports.date', 0 );
    print_rt( $rows );
  } // if

  if ( array_search( 'debug', $options ) !== False )
    print_debug();
?>
