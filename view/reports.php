<?php // Stan 13 июля 2006г.

  $title = "Заключения";
  include '../_local.php';
//check_user( $user, 1, 'rt' );         // Авторизуем пользователя

  $rows = $meta_get->sql_select( '*',
                                 'reports',
                                 'reports.report_pre, reports.report_seq, reports.method, reports.date', 0 );
// print_rt( $rows );

  if ( $rows ) {
    start_table( array( '#',     'id' => 1 ),
                 'Заключение',
                 array( 'Метод', 'td' => 'align=center' ),
                 array( 'Дата',  'td' => 'align=center' ),
                 'Расположение'
               );

    foreach( $rows as $key => $row ) {
      $rid = $row['id'];
      echo_tr( 1,
               link_report( $row ),
               $row['method'],
               sprint_date( $row ),
               ''
             );
    }; // foreach
    stop_table();
  } // if

  if ( array_search( 'debug', $options ) !== False )
    print_debug();
?>
