<?php // Stan 13 июля 2006г.
      //      09 февраля 2010г.

  $title = "Заключения";
  include '../_local.php';
//check_user( $user, 1, 'rt' );         // Авторизуем пользователя

  $rows = $meta_get->sql_select( '*', 'reports', 'report', 0 );
//   $rows2 = $meta_get->sql_select( 'DISTINCT decision', 'reports', 'report' );
// print_rt( $rows2 );

  if ( $rows ) {
    start_table( array( '#',         'id'   => 1 ),
                 array( 'Км',        'td'   => 'align=center' ),
                 'Заключение',
                 array( 'Метод',     'td'   => 'align=center' ),
                 array( 'Дата',      'func' => 'sprint_date' ),
                 array( 'Ур.',       'td'   => 'align=center' ),
                 array( 'Стыков',    'td'   => 'align=center' ),
                 array( 'Годен',     'td'   => 'align=center' ),
                 array( 'Ремонт',    'td'   => 'align=center' ),
                 array( '?',         'td'   => 'align=center' ),
                 'Стыки'
               );

    foreach( $rows as $key => $row ) {
      $rid = $row['id'];
      $A = $meta_get->sql_select_count( 'reports,joints,mjoints', "reports.id='$rid' AND mjoints.decision='ГОДЕН'" );
      $R = $meta_get->sql_select_count( 'reports,joints,mjoints', "reports.id='$rid' AND mjoints.decision='РЕМОНТ'" );
      $x = $meta_get->sql_select_count( 'reports,joints,mjoints', "reports.id='$rid' AND mjoints.decision<>'ГОДЕН' AND mjoints.decision<>'РЕМОНТ'" );

      $joints = $meta_get->sql_append_select( '*', 'reports,joints,mjoints', "reports.id='$rid'" );
      $joints_str = '';
      foreach ( $joints as $joint )
        $joints_str .= sprint_joint( $joint ) . ' [' . sprint_decision( $joint['decision'] ) .
                       '] ' . sprint_dt( $joint ) . ' (' . $joint['welders'] . ')' .
                       "<br />\n";
      echo_tr( 1,
               $row['kp'],
               link_report( $row ),
               $row['method'],
               sprint_date( $row ),
               $row['q_level'],
               $row['joints'],
               $A,
               $R,
               $x,
               $joints_str
             );
    }; // foreach
    stop_table();
  } // if

  if ( array_search( 'debug', $options ) !== False )
    print_debug();
?>
