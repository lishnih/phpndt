<?php // Stan 24 января 2009г.

  $title = "Статистика";
  include '../_local.php';
//check_user( $user, 1, 'rt' );         // Авторизуем пользователя

  include '../lib/lib_ndt/view_joint.php';

  $rows = $meta_get->sql_select( 'distinctrow joints.*', 'reports,mjoints,joints', 'joints.kp,joints.number,joints.type,joints.sign' );
//print_rt( $rows );

  if ( $rows ) {
    start_table( '#', 'Стык', 'Диаметр', 'Толщина', 'Заключения' );

    $i = 1;
    $joint_number = $multiple;
    foreach( $rows as $key => $row ) {
      $diff = (int) ( ( $row['number'] - $joint_number ) / $multiple );
      if ( $diff > 1 ) {
        $str = sprint_color( 'Стыки c ' . sprint_joint( $row, 0, $joint_number ) .
               ' по ' . sprint_joint( $row, 0, $row['number'] - $multiple ) . ' отсутствуют в трассовке', 'Yellow', 'Black' );
        echo '<tr><td><td colspan=4><center><b>' . $str . "</b></center>\n";
      } elseif ( $diff == 1) {
        $str = sprint_color( 'Стык ' . sprint_joint( $row, 0, $joint_number ) .
               ' отсутствует в трассовке', 'Yellow', 'Black' );
        echo '<tr><td><td colspan=4><center><b>' . $str . "</b></center>\n";
      } elseif ( $diff < 0 ) {
        $str = sprint_color( '= = = = =', 'Magenta', 'Black' );
        echo '<tr><td><td colspan=4><center><b>' . $str . "</b></center>\n";
      }; // if

      $isshown = 0;
      $str = '<td><table border=1>';
      $rows2 = $meta_get->sql_append_select( 'reports.*,mjoints.signR,mjoints.signE,mjoints.decision', 'reports,mjoints', 'mjoints.joint_id_=' . $row['id'], 'mjoints.signR,mjoints.signE,date' );
      foreach( $rows2 as $row2 ) {
        if ( $row2['decision'] != 'ГОДЕН' )
          $isshown = 1;
        if ( !$row2['enabled'] )
          $isshown = 1;
        $str .= '<tr><td width=30>' . $row2['signR'] . ' ' . $row2['signE'] .
                '<td width=100>' . $row2['method'] . ': ' . link_report( $row2 ) .
                '<td>' . $row2['q_level'] .
                '<td>' . sprint_date( $row2 ) .
                '<td>' . sprint_decision( $row2['decision'] ) . "\n";
      } // foreach
      $str .= '</table>';

      if ( $isshown ) {
        echo_tr( $i, sprint_joint( $row, 0 ),
                 sprint_diameter( $row ), "'" . sprint_thickness( $row ) );
        echo $str;
        $i++;
      }; // if

      $joint_number = $row['number'] + $multiple;
    }; // foreach

    $str = sprint_color( 'Этот км заканчивается стыком: ' . sprint_joint( $row, 0 ),
           'Magenta', 'Black' );
    echo '<tr><td><td colspan=4><center><b>' . $str . "</b></center>\n";
    stop_table();
  }; // if

  if ( array_search( 'debug', $options ) !== False )
    print_debug();
?>
